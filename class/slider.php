<?php
include_once("db_functions.php");
class slider extends db_functions 
{
	var $todays_date;
	var $tablename			= 	"slider";
	var $primaryKey			= 	"slider_id";
	var $table_fields	  	=   array('slider_id' => "",'slider_name'=>"",'slider_image'=>"");

	function slider()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>