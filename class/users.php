<?php
include_once("db_functions.php");
class users extends db_functions{
     var $tablename = "users";
     var $primaryKey = "user_id";
     var $table_fields = array("user_id"=>"","name"=>"","alias"=>"","email"=>"","img_url"=>"","password"=>"","emirates"=>"","created_date"=>"","status"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>