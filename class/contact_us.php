<?php
include_once("db_functions.php");
class contact_us extends db_functions{
     var $tablename = "contact_us";
     var $primaryKey = "c_id";
     var $table_fields = array("c_id"=>"","c_name"=>"","c_gender"=>"","c_phone"=>"","c_email"=>"","c_msg"=>"","c_file"=>"","c_created"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>