<?php
include_once("db_functions.php");
class flag_more extends db_functions{
     var $tablename = "flag_more";
     var $primaryKey = "fm_id";
     var $table_fields = array("fm_id"=>"","f_id"=>"","fm_type"=>"","fm_url"=>"","fm_thumb"=>"","fm_status"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>