<?php
include_once("db_functions.php");
class pages extends db_functions 
{
	var $todays_date;
	var $tablename			= 	"ar_pages";
	var $primaryKey			= 	"page_id";
	var $table_fields	  	=   array('page_id' => "","parent_id"=>'','page_title'=>"",'page_alias' => "",'page_content' => "",'created_on' => "",'status' => "");

	function pages()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>