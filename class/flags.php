<?php
include_once("db_functions.php");
class flags extends db_functions{
     var $tablename = "flags";
     var $primaryKey = "f_id";
     var $table_fields = array("f_id"=>"","user_id"=>"","f_title"=>"","f_location"=>"","f_created"=>"","f_ip"=>"","f_admin_read_status"=>"","f_status"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>