<?php
include_once("db_functions.php");
class emirates extends db_functions{
     var $tablename = "emirates";
     var $primaryKey = "e_id";
     var $table_fields = array("e_id"=>"","e_name_eng"=>"","e_name_ar"=>"","e_alias"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>