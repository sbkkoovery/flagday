<?php
include_once("db_functions.php");
class settings extends db_functions 
{
	var $todays_date;
	var $tablename			= 	"settings";
	var $primaryKey			= 	"setting_id";
	var $table_fields	  	=   array('setting_id' => "","setting_name"=>'','setting_link'=>"",'status' => "");

	function settings()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>