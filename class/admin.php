<?php
include_once("db_functions.php");
class admin extends db_functions 
{
	var $todays_date;
	var $tablename			= 	"admin";
	var $primaryKey			= 	"aminId";
	var $table_fields	  	=   array('aminId' => "","admin_type"=>"","admin_name"=>'','username'=>"",'password' => "",'email' => "",'flag_access'=>"",'page_access'=>"");

	function admin()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>