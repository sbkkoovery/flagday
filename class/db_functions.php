<?php
	error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
	ini_set('max_execution_time', 500); 
	ini_set('memory_limit', '-1');
	set_time_limit(0);
	require_once 'connect.php';
	date_default_timezone_set('UTC');
	ini_set('date.timezone', 'UTC');
	$ObjConnect 					= 	new connect();
	$dbconnect 						= 	$ObjConnect->db_connect() or trigger_error("SQL", E_USER_ERROR);
	##############	The type of query to be generated for execution.	#############
	define("EXECUTE_SELECT", 1);
	define("EXECUTE_INSERT", 2);
	define("EXECUTE_UPDATE", 3);
	define("EXECUTE_DELETE", 4);
	##############	The type of query to be generated for execution.	##############
	define("OK", 1);
	define("ERROR", -1);
	define("ERROR_ALREADY_EXISTS", -2);
	define("ERROR_INVALID_ARGUMENT", -3);
	define("ERROR_NO_DATA", -4);
	define("ERROR_UNKNOWN_OPTION", -5);
	define("ERROR_DELETION_DENIED", -6);
	define("ERROR_RANGE_EXISTS", -7);
	define("ERROR_INVALID_MOVE", -8);
	//define("E_USER_ERROR","AAAAAAAAAAAAAAAAAAAAA");
	############## This is for default paging style << 1 2 3 4 5 ..... N >> ##############
	define("PAGE_STYLE_DEFAULT", 1);
	############## This is for limit paging style << 1 2 3 4 5 ... L >>		##############
	define("PAGE_STYLE_LIMIT", 1);
	############## This is for previous next style Page 1 of 34 |first | Previous | Next | Last| ##############
	define("PAGE_STYLE_PRENEX", 1);
	
	class db_functions extends connect
	{
		var $table;	          // table name
		var $dbname;            // database name
		var $primarykey;		  // primary key
		var $rows_per_page;     // used in pagination
		var $pageno;            // current page number
		var $lastpage;          // highest page number
		var $arrfields;         // list of fields in this table
		var $data_array;        // data from the database
		var $errors;            // array of error messages
		var $insert_id   = 0;	  
		function db_functions($table,$primarykey,$fields)
		{
			$this->table	       	= 	$table;
			$this->dbname          	= 	$ObjConnect->dbname;
			$this->rows_per_page   	= 	10;
			$this->arrfields 		= 	$fields;
			$this->primarykey		= 	$primarykey;
			$this->insert_id		= 	$insert_id;
		} // constructor
		function build_result_insert($sql)
		{
			 //echo  $sql;
		//	exit;	
			$result 				= 	@mysql_query($sql) or trigger_error("SQL", E_USER_ERROR);
			return OK;
		}
		function build_result($sql)
		{	
		//echo $sql;
			$data_array1 			= 	array();
			$result 				= 	@mysql_query($sql) or trigger_error("SQL", E_USER_ERROR);
			while ($data_array1[] 	= 	mysql_fetch_assoc($result)) 
			{
			
			} // while
		   	mysql_free_result($result);
		  	return $data_array1;
		}
		function build_result_row($sql)
		{
			//echo $sql;
			//exit;
			$result 				= 	@mysql_query($sql) ;//or trigger_error("SQL", E_USER_ERROR);
			if($result)
			{
				$this->data_array 	= 	mysql_fetch_array($result); 
				mysql_free_result($result);
			}
	   		return $this->data_array;
		}
		function execute($values = NULL, $mode = EXECUTE_INSERT, $where = NULL, $order = NULL)
	  	{
			if (($mode == EXECUTE_INSERT || $mode == EXECUTE_UPDATE) &&	(!is_array($values) || count($values)==0))
			{
				return $this->raiseError(ERROR_INVALID_ARGUMENT,array("file" => __FILE__, "line" => __LINE__));
			}
			$values			     = 	$this->_clearArray($values);
			$sql					= 	$this->buildQuery($values, $mode, $where, $order);
			$res  					= 	$this->build_result_insert($sql);
			$this->insert_id 		= 	mysql_insert_id();
			return  OK;
		}
			
		function buildQuery($value = NULL, $mode = EXECUTE_SELECT, $where = FALSE, $order = FALSE)
		{
		//echo $sql;
		//exit;
			//$where				=	mysql_real_escape_string($where);			
			$where				= 	($where) ? " WHERE $where " 		: "";
			//$order				=	mysql_real_escape_string($order);	
			$order					= 	($order) ? " ORDER BY $order " 	: " ORDER BY {$this->primarykey} ";
			//	echo 	$this->table	;
			$temp					= 	"";
			switch ($mode) 
			{
				case EXECUTE_SELECT:
					$temp			= 	implode(",", array_keys($this->arrfields));
					//print_r($temp);
					//exit;
					return "SELECT $temp FROM {$this->table} $where $order ";
				case EXECUTE_UPDATE:
					//$this->alter_walk($value,);
					array_walk($value, 'alter_walk');
					$temp			= 	implode(", ", array_values($value));
					//echo "UPDATE {$this->table} SET $temp $where "; 
					//exit;
					return "UPDATE {$this->table} SET $temp $where ";
				case EXECUTE_INSERT:
					//print_r($value);
					array_walk($value,"myfunction");
					//print_r($value);
					$tempF			= 	implode(",", array_keys($value));
					$tempV			= 	implode("', '", array_values($value));
					//$tempV	= addslashes($tempV);
					//echo "INSERT INTO {$this->table} ($tempF) VALUES ('$tempV')";
					//exit;
					return "INSERT INTO {$this->table} ($tempF) VALUES ('$tempV')";
				case EXECUTE_DELETE:
					return "DELETE FROM {$this->table} $where ";
				default:
			}
		}
		function isExist($where = NULL, $tablename = NULL)
		{
			$where					= 	($where) ? " WHERE $where " : "";
			$tablename				= 	($tablename) ? $tablename : $this->table;
			$sql 					= 	"SELECT count(1) count FROM $tablename $where ;";
			$res  					= 	$this->build_result_row($sql);
			//print_r($res);
			if ($res['count'] >= 1)
			{
				return true;
			}
			return false;
		}
		function insert($array)
		{
			// 	function to check read settings 
			//	if($_SESSION['write_approved']== ""  )	{ check_page_protection('r'); 	}
		//echo $sql;
			return $this->execute($array);
		}	
		function errorlog()
		{
			mysql_query("ROLLBACK");
			$filename 				= 	DIR_ROOT."admin/err_log.doc";
			$file 					= 	fopen($filename, "a") or exit("Unable to open file!");
									
			fwrite($file,"\n\n\n".date("Y-m-d h:i:s A")." \n-----------------------------------\nFile: ".$_SERVER["SCRIPT_NAME"]."\nError: ".mysql_error()."\nError Number: ".mysql_errno());
			fclose($file);
		}	
	    function delete($cond)
	   	{
			return $this->execute($array = "", EXECUTE_DELETE, $cond);
	  	}
		function update($array, $cond)
		{ 
				return  $this->execute($array, EXECUTE_UPDATE, $cond);
		}
		function updateField($array, $cond)
		{
			return   $this->execute($array, EXECUTE_UPDATE, $cond);
		}
	  	function count($where = NULL)
	  	{
			$where					= 	($where) ? " WHERE $where " 		: "";
			$prime					=	$this->primaryKey;
			$sql 					= 	"SELECT count($prime) FROM {$this->table} $where";
				//echo $sql;
			$res  					= 	$this->build_result_row($sql);
			$res  					= 	$res['count('.$prime.')'];
			
			return $res;
		}
		function countRows($sql)
		{
           	//echo $sql;
			//echo "<br>".$sql;
			$rst					=	@mysql_query($sql)or trigger_error("SQxxxL", E_USER_ERROR);
			return $numrows 		        = 	mysql_num_rows($rst);
		}
		function countResultSet($rst)
		{
			return $numrows 		        = 	mysql_num_rows($rst);
		}

		function getMax($where = NULL)
		{
		 	$where					=	($where) ? " WHERE $where " 		: "";
			$sql 					= 	"SELECT max({$this->primaryKey}) FROM {$this->table} $where";
			//echo $sql;
			//exit;
			$res  					=	$this->build_result_row($sql);
			return $res[0];
		}
		function getAll($where = NULL, $order = NULL)
		{
			// function to check read settings 
			//if($_SESSION['read_approved']== "")	{ check_page_protection('r'); 	}
		 	$sql					= 	$this->buildQuery(NULL, EXECUTE_SELECT, $where, $order);
			//
	//echo $sql;
			//exit;
			$res  					= 	$this->build_result($sql);
			return array_filter($res);
		}
		function getFields($fields, $where = NULL, $order = NULL)
		{
		
			$fields					= 	($fields) ? " $fields " 			: " * ";
			$where					= 	($where) ? " WHERE $where " 		: "";
			$order					= 	($order) ? " ORDER BY $order " 	: " ORDER BY {$this->primaryKey} ";
			$sql					= 	"SELECT $fields FROM {$this->table} $where $order ;";
			//echo $sql;
			$res  					= 	$this->build_result($sql);
			$res						=	array_filter($res);
			return $res;
		}
		function getLimit($from, $count, $where = NULL, $order= NULL)
	 	{
		 	//$sql					= 	$this->buildQuery(NULL, EXECUTE_SELECT, $where, $order);
			$where					= 	($where) ? " WHERE $where " 		: "";
			$order					= 	($order) ? " ORDER BY $order " 	: " ORDER BY {$this->primaryKey} ";
			$sql					= 	"SELECT * FROM {$this->table} $where $order  LIMIT $from,$count ";	
			//echo $sql;
			$res					= 	$this->build_result($sql);
			return $res;
		}
		function listQuery($sql)
		{ 
			//echo $sql;
			// 	function to check read settings 
			//	if($_SESSION['read_approved']== ""  )	{ check_page_protection('r'); 	}
			$data_array1 			= 	array();
		//echo $sql;
			//exit;
			$result 				= 	@mysql_query($sql) or trigger_error("SQL", E_USER_ERROR);
			
			while ($data_array1[] 	= 	mysql_fetch_assoc($result)) 
			{
			
			} // while
			
		   	mysql_free_result($result);
		  	return array_filter($data_array1);
		}
		function getSplFields($fields, $where = NULL, $order = NULL)
		{
			$fields					= 	($fields) ? " $fields " 			: " * ";
			$where					= 	($where) ? " WHERE $where " 		: "";
			$order					= 	($order) ? " ORDER BY $order " 	: " ORDER BY {$this->primaryKey} ";
			$sql						= 	"SELECT $fields FROM {$this->table} $where $order ;";
			$res  						= 	$this->build_result($sql);
			return $res;
		}
		function updatestatus($array, $cond)
		{		
			return $this->execute($array, EXECUTE_UPDATE, $cond);
		}
		function getPageLink($pgNo, $totPage, $url, $count = "10")
		{
			$intPre 				= 	$pgNo - 1;
			$intNex 				= 	$pgNo + 1;
			$intFirst 				= 	$pgNo - 1;
			$intLast 				= 	$pgNo + 1;
			//$intLast 	= $totPage;
			//echo $pgNo ;
			//exit;
			if ($intFirst <= 0)
			{
				$intFirst			= 	1;
			}
			if ($intLast >= $totPage)
			{
				$intLast			= 	$totPage;
			}
			if ($intPre <= 0)
			{
				$intPre				= 	1;
				$strReturn			= 	" ";
			}
			else
			{
				$strReturn			= 	str_replace("{pgno}", "$intPre", $url);
				$strReturn			= 	str_replace("{pgtxt}", "<", $strReturn);
			}
			for ($i = $intFirst; $i <= $intLast; $i++)
			{
				if ($i != $pgNo) 
				{
					$strTemp		= 	str_replace("{pgno}", "$i", $url);
					//echo $url;
					//exit;
					if($pgNo+1)
					$strReturn		.= 	str_replace("{pgtxt}", "&nbsp;$i", $strTemp);
					elseif($pgNo+2)
					$strReturn		.= 	str_replace("{pgtxt}", "&nbsp;$i", $strTemp);
					//echo $strReturn;
					//exit;
				}
				else
				{
					$strReturn		.= 	"<span class='current'>$i</span>";
				}
			} 
			if ($intNex > $totPage)
			{
				$intNex				= 	$totPage;
				$strReturn			.= 	" ";
			}
			else
			{
				$strTemp			= 	str_replace("{pgno}", "$intNex", $url);
				$strReturn 			.= 	str_replace("{pgtxt}", ">", $strTemp);
			}
		   	$strReturn				.=	"<span class=extend>...</span>".str_replace(array("{pgtxt}","{pgno}"),array("Last","$totPage"),$url);
			return $strReturn;
		}
		function getOne($sql)
		{
			$res  					= 	$this->build_result($sql);
			return $res;
		}
		function getRow($where = NULL, $order = NULL)
		{
			//echo $where;
			 $sql 					= 	$this->buildQuery(NULL, EXECUTE_SELECT, $where, $order);
 			// echo $sql;
			//exit;
			$res  					= 	$this->build_result_row($sql);
			//print_r($res);
			return $res;
		}
		function getRowSql($sql)
		{
			//echo $where;
			//$sql 	= $this->buildQuery(NULL, EXECUTE_SELECT, $where, $order);
 			//echo $sql;
			$res  					= 	$this->build_result_row($sql);
			return $res;
		}
		function getRowcat($where = NULL, $order = NULL)
		{
			//echo $where;
			$sql 					= 	$this->buildQuery(NULL, EXECUTE_SELECT, $where, $order);
			//echo $sql;
			$res  					= 	$this->build_result_row($sql);
			//echo $res['category_name'];
			//exit;
			return $res['category_name'];
		}
		function getOptions($strKey, $strVal, $selected = NULL, $where = NULL, $order = NULL)
		{

			$strOptions 			= 	"";
			$where					= 	($where) ? " WHERE $where " 		: "";
			$order					= 	($order) ? " ORDER BY $order " 	: " ORDER BY {$this->primarykey} ";
		 	$sql 					= 	"SELECT * FROM {$this->table} $where $order";
		 	$res  					= 	$this->build_result($sql);
			//$pkey 				= 	$this->primarykey;
			foreach ($res as $key => $val)
			{
				//echo $val[$this->primarykey];
				if($val[$this->primarykey]<>"")
				{
					$strOptions 	.= 	"<option value='{$val[$strKey]}' ".(($val[$strKey] == $selected) ? "selected" : "").">{$val[$strVal]}</option>\r\n";
				}
			}
			return $strOptions;
		}
		function delete_new($my_sql)
		{ 
			//echo $my_sql;
			mysql_query($my_sql);
		}
		function simpleupdate($my_sql)
		{
			//return $this->insert_id;
			//echo $my_sql;
			$resultup 				= 	mysql_query($my_sql);//or trigger_error("SQL", E_USER_ERROR);
			return $resultup;
		}
		function insertId()
		{
			return $this->insert_id;
		}
		function random_value($startVal	= 0, $endVal = 1000)
		{
			$time 					= 	time();
			srand ((double) microtime() * 1000000); //to generate random no. between 0 and 100
			$randval 				= 	rand();
			$time 					.= 	rand($startVal, $endVal);
			return $time;
		}
		function _clearArray($array)
		{
			if (!is_array($array) || count($array)==0)
			{
				//echo "empty";
				return $this->raiseError(ERROR_INVALID_ARGUMENT, array("file" => __FILE__, "line" => __LINE__));
			}
			foreach ($array as $key => $val)
			{
				//print_r($this->arrfields);
				//echo $this->fields;
				if (!array_key_exists($key, $this->arrfields))
				{
					unset($array[$key]);
				}
				else
				{
					$array[$key]	=	mysql_real_escape_string($array[$key]);
				}
			}
		//	print_r($array);
			return $array;
		}
		function raiseError($code, $info = array())
		{
			$message				= 	$this->errorMessage($code);
			return $message ;
		}
		function errorMessage($code)
		{
			static $errorMessages;
			if (!isset($errorMessages)) 
			{
				$errorMessages 	= 	array(
											ERROR						=> "unknown errors",
											ERROR_ALREADY_EXISTS		=> "The data you entered already exists",
											ERROR_INVALID_ARGUMENT		=> "Invalid argument",
											ERROR_NO_DATA				=> "No data exists",
											ERROR_UNKNOWN_OPTION		=> "Unknown option",
											ERROR_DELETION_DENIED		=> "Deletion denied for the current record",
											OK							=> "No error",
											ERROR_RANGE_EXISTS			=> "The range already exists. Please check the range.",
											ERROR_INVALID_MOVE			=> "Updation denied: Invalid move"
										);
			}
			if (array_key_exists($code, $errorMessages)) 
			{
				return $errorMessages[$code];
			}
			return $errorMessages["ERROR"];		
		}
				
		function max($sql)
		{
			return	$this->build_result_row("select max(priority) as max_count from {$this->table}");
		}
		function max_id($priority)
		{
			return	$this->build_result_row("select max($priority) as max_id from {$this->table}");
		}
	
	} // end class
	function alter_walk(&$val, $key)
	{
		$val 				= 	"$key = '$val'";
	}
	function myfunction(&$value,$key) 
	{
		$value 				= 	addslashes($value);
		//echo $value;
	}
	function search_results($sql)
	{
		return	$this->build_result($sql);
	}
?>
