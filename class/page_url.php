<?php
include_once("db_functions.php");
class page_url extends db_functions 
{
	var $todays_date;
	var $tablename			= 	"page_url";
	var $primaryKey			= 	"pu_id";
	var $table_fields	  	=   array('pu_id' => "","pu_name"=>"");

	function page_url()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>