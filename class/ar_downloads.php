<?php
include_once("db_functions.php");
class downloads extends db_functions{
     var $tablename = "ar_downloads";
     var $primaryKey = "d_id";
     var $table_fields = array("d_id"=>"","d_name"=>"","d_alias"=>"","d_image"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>