<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."class/forgot_password.php");
$objForgot						 =	new forgot_password();
$requestStr						=	$objCommon->esc($_GET['requestStr']);
if($requestStr){
	$getForgotDetails			  =	$objForgot->getRow("fp_string='".$requestStr."'");
}
if($getForgotDetails['user_id'] == ''){
	header("location:".SITE_ROOT);
	exit;
}
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.validate.js"></script>
<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
<div class="background_div" style="background-image:url('<?php echo SITE_ROOT.'uploads/home_slider/'.$getHomeSlider['slider_image']?>');">
	<div class="white_overlay">
		<div class="container">
			<div class="container_contact">
			<div class="contact_us">
				<div class="contact_head">
				<?php echo $objCommon->displayMsg();?>
					<h3>هل نسيت كلمة المرور</h3>
				</div>
				<div class="contact_us_form">
					<form id="submit_forgot_password_request" method="post"  action="<?php echo SITE_ROOT?>access/forgot_password_form.php?requestStr=<?php echo $requestStr?>">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									 <label>كلمة السر</label>
                    				 <input type="password" class="form-control" placeholder="الاسم الكامل" name="u_password" id="u_password">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									 <label>كرر كلمة السر</label>
									<input type="password" class="form-control" placeholder="الاسم الكامل" name="u_con_password" id="u_con_password">
								</div>
							</div>
						</div>
						<div class="row">
							 <div class="col-sm-12">
							 	<input type="hidden" name="langSwitch" value="ar" />
								<button type="submit"  class="btn btn-default submit_contact">عرض</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(e) {
	$("#job_image").filestyle();
	$("#submit_forgot_password_request").validate({
			rules: {
				u_password: {required:true,minlength: 6}, 
				u_con_password: { equalTo: "#u_password" }
			},
			messages: {
				u_password: {required:'Can\'t be empty',minlength:'Password must be at least 6 characters long'},
				u_con_password:'Please enter the same password as above'
			}
		});
});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>