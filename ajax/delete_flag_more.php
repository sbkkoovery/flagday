<?php
@session_start();
$LANGUAGE				   =	'ar_';
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/flags.php");
include_once(DIR_ROOT."class/flag_more.php");
$objCommon				  =	new common();
$objFlags				   =	new flags();
$objFlagMore				=	new flag_more();
$userId					 =	$_SESSION['userId'];
$delId					  =	$objCommon->esc($_GET['delId']);
if($delId != '' && $userId != ''){
	$getFlagDetails		 =	$objFlagMore->getRowSql("SELECT more.*,flag.f_id
														 FROM flag_more  AS more 
														 LEFT JOIN flags AS flag ON more.f_id = flag.f_id
														 WHERE more.fm_id =".$delId." AND flag.user_id = ".$userId);
	if($getFlagDetails['fm_id']){
		$objFlagMore->delete("fm_id=".$getFlagDetails['fm_id']);
		unlink(DIR_ROOT."uploads/flags_images/".$getFlagDetails['fm_url']);
		if($getFlagDetails['fm_thumb'] != ''){
			unlink(DIR_ROOT."uploads/flags_images/".$getFlagDetails['fm_url']);
		}
		$countParentflag	=	$objFlags->count("f_id=".$getFlagDetails['f_id']);
		if($countParentflag==0){
			$objFlags->delete("f_id=".$getFlagDetails['f_id']);
		}
	}
}
?>