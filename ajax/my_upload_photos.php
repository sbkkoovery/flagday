<?php
@session_start();
$LANGUAGE				   =	'ar_';
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/flags.php");
$objCommon				  =	new common();
$objFlags				   =	new flags();
$getUserDetails			 =	$objFlags->getRowSql("SELECT user.*,emirates.e_name_ar  FROM users AS user LEFT JOIN emirates ON user.emirates = emirates.e_id  WHERE  user.user_id=".$_SESSION['userId']);
$getMyFlags				 =	$objFlags->listQuery("SELECT more.*,flag.f_title,flag.f_location,flag.user_id,flag.f_created
														 FROM flag_more  AS more 
														 LEFT JOIN flags AS flag ON more.f_id = flag.f_id
														 WHERE  flag.user_id = ".$_SESSION['userId']." ORDER BY flag.f_created desc");
$userImg					=	($getUserDetails['img_url'] !='')?$getUserDetails['img_url']:'no-image.jpg';

?>
<div class="photo_gallery">
	<div class="row grid">
		<?php
		if(count($getMyFlags) >0){
			foreach($getMyFlags as $allMyFlags){
				$uploadType		 =	$allMyFlags['fm_type'];
				if($uploadType ==1){
					$flag_images	=	$allMyFlags['fm_url'];
				}else if($uploadType ==2){
					$flag_url	   =	$allMyFlags['fm_url'];
					$flag_images	=	$objCommon->getThumb($allMyFlags['fm_thumb']);
				}
		?>
		<div class="col-sm-2 col-lg-3 grid-item">
			<div class="item-sec">
				<div class="img-sec-masnory">
					<div class="edit_delete text-center" data-delid="<?php echo $objCommon->html2text($allMyFlags['fm_id'])?>"><i class="fa fa-trash"></i></div>
					<?php
					if($uploadType ==1){
						?>
					<a href="<?php echo SITE_ROOT.'uploads/flags_images/'.$flag_images?>" class="mylightbox"   title="<?php echo $objCommon->html2text($allMyFlags['f_title'])?>"><img class="img-responsive" src="<?php echo SITE_ROOT.'uploads/flags_images/'.$flag_images?>"></a>
					<?php
					}else if($uploadType ==2){
					?>
					<a href="JavaScript:html5Lightbox.showLightbox(2, '<?php echo SITE_ROOT?>uploads/flags_images/<?php echo $flag_url?>', '<?php echo $objCommon->html2text($allMyFlags['f_title'])?>', 800, 450, '<?php echo SITE_ROOT?>uploads/flags_images/<?php echo $flag_url?>');"><img class="img-responsive" src="<?php echo SITE_ROOT.'uploads/flags_images/'.$flag_images?>"><span class="videoIcon"><i class="fa fa-video-camera"></i></span></a>
					<?php
					}
					?>
				</div>
				<div class="title_info">
					<div class="title">
						<h3><?php echo $objCommon->limitWords($allMyFlags['f_title'],60)?></h3>
					</div>
					<div class="user-details">
						<div class="img-sec-user">
							 <img class="img-responsive" src="<?php echo SITE_ROOT.'uploads/profile/'.$userImg?>">
						</div>
						<div class="user_info_name">
							<h3><?php echo $objCommon->html2text($getUserDetails['name'])?></h3>
							<p class="map_marker"><?php echo $objCommon->html2text($allMyFlags['f_location'])?><i class="fa fa-map-marker"></i></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
			}
		}else{?>
        	<div class="col-sm-12">
                        	<div class="nothingfound">
                                
                            </div>
                        </div>
		<?php }
		?>

	</div>
</div>
<script>
$(".edit_delete").on("click",function(){
	var that				=	this;
		delId			   =	$(this).data("delid");
		elem 				=	$(this).closest('.item');
	$.confirm({
				'title'	  : 'حذف التأكيد',
				'message'	: 'أنت على وشك حذف هذا العنصر ؟',
				'buttons'	: {
				'نعم'		: {
				'class'	  : 'blue',
				'action'	 : function(){
				elem.slideUp();
				if(delId){
					$.get('<?php echo SITE_ROOT?>ajax/delete_flag_more.php',{'delId':delId},function(data){
						loadMyUploads();
					});
				};
				}
				},
				'لا'	: {
				'class'	: 'gray',
				'action': function(){
					$(".review_action_drop").slideUp();
				}	// Nothing to do in this case. You can as well omit the action property.
				}
				}
			});
});
</script>