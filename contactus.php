<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
?>
	<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.validate.js"></script>
	<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
<div class="background_div" style="background-image:url('<?php echo SITE_ROOT.'uploads/home_slider/'.$getHomeSlider['slider_image']?>');">
	<div class="white_overlay">
		<div class="container">
			<div class="container_contact">
			<div class="contact_us">
				<div class="contact_head">
				<?php echo $objCommon->displayMsg();?>
					<h3>تواصــل معنا</h3>
					<p>واسمحوا لنا أن نتعرف على الاقتراحات و الاستفسارات الخاصة بكم</p>
				</div>
				<div class="contact_us_form">
					<form id="submit_contact_us" method="post" enctype="multipart/form-data" action="<?php echo SITE_ROOT?>access/contact_us.php">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label >الجنس</label>
									<select class="form-control" name="gender" id="gender">
										<option value="">الجنس</option>
										<option value="male">ذكر</option>
										<option value="female">أنثى</option>
									</select>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									 <label >الإسم</label>
									<input type="text" class="form-control" name="name" id="name" placeholder="الإسم">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label >رقم الهاتف</label>
									<input type="text" class="form-control fontSans" name="phone" id="phone" placeholder="رقم الهاتف">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									 <label >عنوان البريد الإلكتروني</label>
									<input type="email" class="form-control fontSans"  name="email" id="email" placeholder="عنوان البريد الإلكتروني">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									 <label >الرسالة</label>
									<textarea class="form-control" placeholder="الرسالة" name="msg" id="msg"></textarea>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									 <label >المرفقات</label>
									<input type="file" id="job_image" name="attachment" >
								</div>
							</div>
							 <div class="col-sm-12">
								<button type="submit" id="contactUs" class="btn btn-default submit_contact">عرض</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(e) {
	$("#job_image").filestyle({
			buttonText : "اختر من"
	});
	$("#submit_contact_us").validate({
		rules: {
			name: "required",
			msg: "required",
			email:{required:true,email: true}
		},
		messages: {
			name: 'Can\'t be empty',
			msg: 'Can\'t be empty',
			email: {required:'Can\'t be empty',email:'Please enter valid email address'}
		}

	});
});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>