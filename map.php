<?php
@session_start();
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."class/emirates.php");
$objEmirates				 =	new emirates();
$getAllEmirates			  =	$objEmirates->getAll("","e_id");
$getPage			   		 =	$objEmirates->getRowSql("SELECT * FROM ".$LANGUAGE."pages WHERE page_id=2");
$partcipates				 =	$objEmirates->listQuery("SELECT count(user_id) AS participates,emirates FROM  users WHERE status=1 GROUP BY  emirates ");
foreach($partcipates AS $allParticipates){
	$arrParticipate[$allParticipates['emirates']] =	$allParticipates['participates'];
}
$imageAbu					=	$objEmirates->getRowSql("SELECT more.fm_id,more.fm_url,more.fm_thumb,more.fm_type FROM flags AS flag LEFT JOIN flag_more  AS more ON flag.f_id = more.f_id WHERE flag.f_status=1 AND more.fm_status=1 AND flag.f_location LIKE '%abu dhabi%' GROUP BY flag.f_id  ORDER BY flag.f_created desc LIMIT 1");
if($imageAbu['fm_id']){
	if($imageAbu['fm_type'] ==1){
		$imgAbu		 	  =	SITE_ROOT.'uploads/flags_images/'.$imageAbu['fm_url'];
	}else if($imageAbu['fm_type'] ==2){
		$imgAbu		 	  =	SITE_ROOT.'uploads/flags_images/'.$objCommon->getThumb($imageAbu['fm_thumb']);
	}
}else{
	$imgAbu				  =	SITE_ROOT.'images/abudhabi.jpg';
}
$imageDub					=	$objEmirates->getRowSql("SELECT more.fm_id,more.fm_url,more.fm_thumb,more.fm_type FROM flags AS flag LEFT JOIN flag_more  AS more ON flag.f_id = more.f_id WHERE flag.f_status=1 AND more.fm_status=1 AND flag.f_location LIKE '%dubai%' GROUP BY flag.f_id  ORDER BY flag.f_created desc LIMIT 1");
if($imageDub['fm_id']){
	if($imageDub['fm_type'] ==1){
		$imgDub		 	  =	SITE_ROOT.'uploads/flags_images/'.$imageDub['fm_url'];
	}else if($imageDub['fm_type'] ==2){
		$imgDub		 	  =	SITE_ROOT.'uploads/flags_images/'.$objCommon->getThumb($imageDub['fm_thumb']);
	}
}else{
	$imgDub				  =	SITE_ROOT.'images/dubai.jpg';
}
$imageSha					=	$objEmirates->getRowSql("SELECT more.fm_id,more.fm_url,more.fm_thumb,more.fm_type FROM flags AS flag LEFT JOIN flag_more  AS more ON flag.f_id = more.f_id WHERE flag.f_status=1 AND more.fm_status=1 AND flag.f_location LIKE '%sharjah%' GROUP BY flag.f_id  ORDER BY flag.f_created desc LIMIT 1");
if($imageSha['fm_id']){
	if($imageSha['fm_type'] ==1){
		$imgSha		 	  =	SITE_ROOT.'uploads/flags_images/'.$imageSha['fm_url'];
	}else if($imageSha['fm_type'] ==2){
		$imgSha		 	  =	SITE_ROOT.'uploads/flags_images/'.$objCommon->getThumb($imageSha['fm_thumb']);
	}
}else{
	$imgSha				  =	SITE_ROOT.'images/Sharjah.jpg';
}
$imageAjm					=	$objEmirates->getRowSql("SELECT more.fm_id,more.fm_url,more.fm_thumb,more.fm_type FROM flags AS flag LEFT JOIN flag_more  AS more ON flag.f_id = more.f_id WHERE flag.f_status=1 AND more.fm_status=1 AND flag.f_location LIKE '%ajman%' GROUP BY flag.f_id  ORDER BY flag.f_created desc LIMIT 1");
if($imageAjm['fm_id']){
	if($imageAjm['fm_type'] ==1){
		$imgAjm		 	  =	SITE_ROOT.'uploads/flags_images/'.$imageAjm['fm_url'];
	}else if($imageAjm['fm_type'] ==2){
		$imgAjm		 	  =	SITE_ROOT.'uploads/flags_images/'.$objCommon->getThumb($imageAjm['fm_thumb']);
	}
}else{
	$imgAjm				  =	SITE_ROOT.'images/ajman.jpg';
}
$imageUmm					=	$objEmirates->getRowSql("SELECT more.fm_id,more.fm_url,more.fm_thumb,more.fm_type FROM flags AS flag LEFT JOIN flag_more  AS more ON flag.f_id = more.f_id WHERE flag.f_status=1 AND more.fm_status=1 AND flag.f_location LIKE '%umm al quwain%' GROUP BY flag.f_id  ORDER BY flag.f_created desc LIMIT 1");
if($imageUmm['fm_id']){
	if($imageUmm['fm_type'] ==1){
		$imgUmm		 	  =	SITE_ROOT.'uploads/flags_images/'.$imageUmm['fm_url'];
	}else if($imageUmm['fm_type'] ==2){
		$imgUmm		 	  =	SITE_ROOT.'uploads/flags_images/'.$objCommon->getThumb($imageUmm['fm_thumb']);
	}
}else{
	$imgUmm				  =	SITE_ROOT.'images/ummul.jpg';
}
$imageRas					=	$objEmirates->getRowSql("SELECT more.fm_id,more.fm_url,more.fm_thumb,more.fm_type FROM flags AS flag LEFT JOIN flag_more  AS more ON flag.f_id = more.f_id WHERE flag.f_status=1 AND more.fm_status=1 AND flag.f_location LIKE '%ras al-khaimah%' GROUP BY flag.f_id  ORDER BY flag.f_created desc LIMIT 1");
if($imageRas['fm_id']){
	if($imageRas['fm_type'] ==1){
		$imgRas		 	  =	SITE_ROOT.'uploads/flags_images/'.$imageRas['fm_url'];
	}else if($imageRas['fm_type'] ==2){
		$imgRas		 	  =	SITE_ROOT.'uploads/flags_images/'.$objCommon->getThumb($imageRas['fm_thumb']);
	}
}else{
	$imgRas				  =	SITE_ROOT.'images/ras.jpg';
}
$imageFuj					=	$objEmirates->getRowSql("SELECT more.fm_id,more.fm_url,more.fm_thumb,more.fm_type FROM flags AS flag LEFT JOIN flag_more  AS more ON flag.f_id = more.f_id WHERE flag.f_status=1 AND more.fm_status=1 AND flag.f_location LIKE '%fujairah%' GROUP BY flag.f_id  ORDER BY flag.f_created desc LIMIT 1");
if($imageFuj['fm_id']){
	if($imageFuj['fm_type'] ==1){
		$imgFuj		 	  =	SITE_ROOT.'uploads/flags_images/'.$imageFuj['fm_url'];
	}else if($imageFuj['fm_type'] ==2){
		$imgFuj		 	  =	SITE_ROOT.'uploads/flags_images/'.$objCommon->getThumb($imageFuj['fm_thumb']);
	}
}else{
	$imgFuj				  =	SITE_ROOT.'images/fujaieha.jpg';
}
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.validate.js"></script>
<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function PreviewImage(no) {
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("uploadImage"+no).files[0]);
		
		oFReader.onload = function (oFREvent) {
			document.getElementById("uploadPreview"+no).src = oFREvent.target.result;
		};
	}
</script>
<div class="background_div" style="background-image:url('<?php echo SITE_ROOT.'uploads/home_slider/'.$getHomeSlider['slider_image']?>');">
	<div class="white_overlay">
		<div class="container alt-width-cont">
			<div class="map-section">
				<img class="img-responsive" src="images/mapuae-compressor.png" />
				<span class="map-markers abudhabi" data-emirates="abu-dhabi" data-images="<?php echo $imgAbu?>" data-count=" شارك : <?php echo ($arrParticipate[1])?$arrParticipate[1]:'0'?>">
					<span class="emirites">أبوظبي</span>
				  </span>
				 <span class="map-markers dubai" data-emirates="dubai" data-images="<?php echo $imgDub?>" data-count=" شارك : <?php echo ($arrParticipate[3])?$arrParticipate[3]:'0'?>">
					 <span class="emirites">دبي</span>
				 </span>
				 <span class="map-markers sharjah" data-emirates="sharjah" data-images="<?php echo $imgSha?>" data-count=" شارك : <?php echo ($arrParticipate[6])?$arrParticipate[6]:'0'?>">
					 <span class="emirites">الشارقة</span>
				 </span>
				 <span class="map-markers ajman" data-emirates="ajman" data-images="<?php echo $imgAjm?>" data-count=" شارك : <?php echo ($arrParticipate[2])?$arrParticipate[2]:'0'?>">
					 <span class="emirites">عجمان</span>
				 </span>
				 <span class="map-markers ummulquym" data-emirates="umm-al-quwain"  data-images="<?php echo $imgUmm?>" data-count=" شارك : <?php echo ($arrParticipate[7])?$arrParticipate[7]:'0'?>">
					 <span class="emirites">أم القيوين</span>
				 </span>
				 <span class="map-markers rasalkaimah" data-emirates="ras-al-khaimah" data-images="<?php echo $imgRas?>" data-count=" شارك : <?php echo ($arrParticipate[5])?$arrParticipate[5]:'0'?>">
					 <span class="emirites">رأس الخيمة</span>
				 </span>
				 <span class="map-markers fujairah" data-emirates="fujairah" data-images="<?php echo $imgFuj?>" data-count=" شارك : <?php echo ($arrParticipate[4])?$arrParticipate[4]:'0'?>">
					 <span class="emirites">الفجيرة</span>
				 </span>
				 <div class="clearfix"></div>
			</div>
			<div class="registration_section">
				<div class="head_registration">
					<h1><?php echo $objCommon->html2text($getPage['page_title'])?></h1>
				</div>
				<?php echo $objCommon->html2text($getPage['page_content'])?>
				<?php
				if($_SESSION['userId']){
					?>
					<a href="<?php echo SITE_ROOT?>profile" class="viewProfile">شارك في يوم العلم <i class="fa fa-chevron-left"></i></a>
					<?php
				}else{
					?>
					<a href="javascript:;" data-toggle="modal" data-target=".bs-example-modal-sm">شارك في يوم العلم <i class="fa fa-chevron-left"></i></a>
					<?php
				}
				?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
		 //$(".nav_section_slides").perfectScrollbar();
		
		$(".close1").click(function(){
		$(".nav_section_slides").removeClass("nav_toggle");
		});
		$(".map-markers").append(
								 '<span class="popup-map">'+
								 '<span class="arw"></span>'+
								 '<span class="img-sec-pop">'+
								 '<img class="img-responsive">'+
								 '<span class="shadow">'+
								 '<a href="javascript:;" onClick="return false" class="go_on">'+
								 '<i class="fa fa-chevron-left"></i>'+
								 '</a>'+
								 '<div class="info-pop">'+
								 '<p class="emiritesz" id="emrite">أبوظبي</p>'+
								 '<p class="membercount" id="membrcount"> 12345  شارك</p>'+
								 '</div>'+
								 '</span>'+
								 '</span>'+
								 '</span>'
								);
		
		$(".map-markers").mouseenter(function(){
			var image_link		=	$(this).attr('data-images');
				people_count	=	$(this).attr('data-count');
				popupCount		=	$(this).children('.popup-map');
				popups			=	parseInt($(this).css('top'));
				emrite			=	$(this).children('.emirites').html();
				count			=	$(this).attr('data-count');
				winwidth		=	$(window).width();
				//alert(popups);
				if(popups <	100){
					var popupschange 	=	$(this).children('.popup-map').addClass('popup-map-alt');
					popupschange.children('span.arw').addClass('arw-alt');
				}
				popupCount.children().children('img').attr('src', image_link);
				popupCount.find('#emrite').html(emrite);
				popupCount.find('#membrcount').html(count);
				$('.popup-map').not($(this).children('.popup-map')).hide();
				$(this).children('.popup-map').show();
				
				
		});
		$(".go_on, .popup-map, .shadow").click(function(){
			var emirates=$(this).parents(".map-markers").data("emirates");
			window.location.href='<?php echo SITE_ROOT?>flags/emirates/'+emirates;
			 
		})
		$(".map-markers").mouseleave(function(){
			$('.popup-map').hide();
		});
		$("#LoginModalForm").validate({
			rules: {
				log_password: "required",
				log_email:{required:true,email: true}
			},
			messages: {
				log_password: 'Can\'t be empty',
				log_email: {required:'Can\'t be empty',email:'Please enter valid email address'}
			}
		});
		$("#registerModalForm").validate({
			rules: {
				u_name: "required",
				u_emirates: "required",
				u_email:{required:true,email: true},
				u_password: {required:true,minlength: 6}, 
				u_con_password: { equalTo: "#u_password" }
			},
			messages: {
				u_name: 'Can\'t be empty',
				u_emirates: 'Can\'t be empty',
				u_email: {required:'Can\'t be empty',email:'Please enter valid email address'},
				u_password: {required:'Can\'t be empty',minlength:'Password must be at least 6 characters long'},
				u_con_password:'Please enter the same password as above'
			}
		});
	});
	
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>

<div class="modal fade bs-example-modal-sm loginModel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">شارك في يوم العلم</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo SITE_ROOT?>access/login.php" method="post" id="LoginModalForm">
			<?php echo $objCommon->displayMsg();?>
            <div class="form-group">
                <label>اسم المستخدم</label>
                <input type="email" class="form-control"  name="log_email" id="log_email" placeholder="اسم المستخدم">
            </div>
            <div class="form-group">
                <label>كلمة السر</label>
                <input type="password" class="form-control" name="log_password" id="log_password" placeholder="كلمة السر">
            </div>
            <a href="<?php echo SITE_ROOT?>forgot-password">هل نسيت كلمة المرور؟</a>
			<input type="hidden" name="langSwitch" value="ar" />
            <button type="submit" class="btn btn-primary login">تسجيل الدخول</button>
        </form>
       
      </div>
      <div class="register_new">
           <a href="javascript:;" data-toggle="modal" data-target="#registers">مستخدم جديد؟</a>
       </div>
    </div>
  </div>
</div>
<div class="modal fade resisterModal" id="registers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">شارك في يوم العلم</h4>
      </div>
      <div class="modal-body">
	  <?php
	 if($_SESSION['register_err'] ==1){
	  echo '<div class="alert alertClose alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alertClose" aria-label="Close"><span aria-hidden="true">&times;</span></button> Registered email already exists</div><script>$( ".alertClose" ).delay( 4000 ).slideUp( 400 );</script>';
	 }else if($_SESSION['register_err'] ==2){
		 echo '<div class="alert alertClose alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alertClose" aria-label="Close"><span aria-hidden="true">&times;</span></button>Please fill the required fields</div><script>$( ".alertClose" ).delay( 4000 ).slideUp( 400 );</script>';
	 }
	  ?>
       <div class="profile-pic">
       		<form action="<?php echo SITE_ROOT?>access/register.php" method="post" enctype="multipart/form-data" id="registerModalForm" >
            <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                        <label>الاسم الكامل</label>
                        <input type="text" class="form-control" name="u_name" id="u_name" placeholder="الاسم الكامل">
                    </div>
                    <div class="form-group">
                        <label>البريد الإلكتروني</label>
                        <input type="email" class="form-control" name="u_email" id="u_email" placeholder="البريد الإلكتروني">
                    </div>
              </div>
            <div class="col-sm-4">
                <div class="profie-pics"> 
                    <img id="uploadPreview1" src="images/no-image.jpg"/><br />
                    <div class="fileUpload btn btn-primary">
                        <span>Upload</span>
                        <input id="uploadImage1" type="file" class="upload" name="u_image"  onchange="PreviewImage(1);" style="" />
                    </div>
              </div>
          </div>
          
       </div>
       <div class="row margin-it">
		   <div class="col-sm-6">
				<div class="form-group">
						<label>اعد كتابة كلمة المرور الجديدة</label>
						<input type="password" class="form-control" placeholder="اعد كتابة كلمة المرور الجديدة" name="u_con_password" id="u_con_password">
					</div>
				</div>
       		<div class="col-sm-6">
            	<div class="form-group">
                    <label>كتابة كلمة المرور الجديدة</label>
                    <input type="password" class="form-control" placeholder="كتابة كلمة المرور الجديدة" name="u_password" id="u_password">
                </div>
            </div>
            <div class="col-sm-12">
            <div class="form-group">
                    <label>الإمارات</label>
					<select name="u_emirates"  id="u_emirates" class="form-control">
						<option value="">الإمارات</option>
						<?php
						foreach($getAllEmirates as $allEmirates){
						?>
						<option value="<?php echo $allEmirates['e_id']?>"><?php echo $objCommon->html2text($allEmirates['e_name_ar'])?></option>
						<?php
						}
						?>
					</select>
                </div>
            </div>
            <div class="col-sm-12">
				<input type="hidden" name="langSwitch" value="ar" />
            	<button type="subject" class="btn btn-primary loginReg">تسجيل</button>
            </div>
       </div>
       </form>
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>-->
    </div>
  </div>
</div>
<?php
if($_SESSION['login_err']==1 ){
?>
	<script type="text/javascript">
		$(".loginModel").modal('show');
	</script>
<?php
	unset($_SESSION['login_err']);
}

if($_SESSION['register_err'] != ''){
?>
	<script type="text/javascript">
		$(".resisterModal").modal('show');
	</script>
<?php
	unset($_SESSION['register_err']);
}
?>