
<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."includes/header_en.php");
include_once(DIR_ROOT."class/flags.php");
$objFlags				   =	new flags();
$emirates				   =	$objCommon->esc($_GET['emirates']);
$emiratesArr		  		=	array('abu-dhabi'=>"abu dhabi",'dubai'=>"dubai",'sharjah'=>"sharjah",'ajman'=>"ajman",'umm-al-quwain'=>"umm al quwain",'ras-al-khaimah'=>"ras al-khaimah",'fujairah'=>"fujairah");
$limit 					  =	20;
$page 					   =	(int) (!isset($_GET['p'])) ? 1 : $_GET['p'];
$sqlImagesList			  =	"SELECT more.*,flag.f_title,flag.f_location,flag.user_id,flag.f_created,user.name,user.img_url,user.alias
														 FROM flag_more  AS more 
														 LEFT JOIN flags AS flag ON more.f_id = flag.f_id
														 LEFT JOIN users AS user ON flag.user_id = user.user_id
														 WHERE flag.f_status=1 AND more.fm_status=1 AND flag.f_location LIKE '%".$emiratesArr[$emirates]."%'  ORDER BY flag.f_created desc";
$start 					 =	($page * $limit) - $limit;
if(($objFlags->countRows($sqlImagesList)) > ($page * $limit) ){
	$next 				  =	++$page;
}
$getFlags			 	  =	$objFlags->listQuery($sqlImagesList. " LIMIT ".$start.", ".$limit);

?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/masonry.pkgd.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/imagesloaded.pkgd.js"></script>
<div class="background_div_new " style="background-image:url('<?php echo SITE_ROOT.'uploads/home_slider/'.$getHomeSlider['slider_image']?>');">
	<div class="white_overlay_new">
		<h2 class="emirateHead"><?php echo ucfirst($emiratesArr[$emirates])?></h2>
		<div class="row grid">
		<?php
		if(count($getFlags) >0){
		foreach($getFlags as $allFlags){
			$userImg					=	($allFlags['img_url'] !='')?$allFlags['img_url']:'no-image.jpg';
			$uploadType		  		 =	$allFlags['fm_type'];
			if($uploadType ==1){
				$flag_images			=	$allFlags['fm_url'];
			}else if($uploadType ==2){
				$flag_url	   		   =	$allFlags['fm_url'];
				$flag_images			=	$objCommon->getThumb($allFlags['fm_thumb']);
			}
		?>
			<div class="col-sm-2 col-lg-sp-5 grid-item">
				<div class="item-sec">
				<div class="img-sec-masnory">
					<?php
						if($uploadType ==1){
							?>
						<a href="<?php echo SITE_ROOT.'uploads/flags_images/'.$flag_images?>" class="mylightbox"   title="<?php echo $objCommon->html2text($allFlags['f_title'])?>"><img class="img-responsive" src="<?php echo SITE_ROOT.'uploads/flags_images/'.$flag_images?>"></a>
						<?php
						}else if($uploadType ==2){
						?>
						<a href="JavaScript:html5Lightbox.showLightbox(2, '<?php echo SITE_ROOT?>uploads/flags_images/<?php echo $flag_url?>', '<?php echo $objCommon->html2text($allFlags['f_title'])?>', 800, 450, '<?php echo SITE_ROOT?>uploads/flags_images/<?php echo $flag_url?>');"><img class="img-responsive" src="<?php echo SITE_ROOT.'uploads/flags_images/'.$flag_images?>"><span class="videoIcon"><i class="fa fa-video-camera"></i></span></a>
						<?php
						}
					?>
				</div>
				<div class="title_info">
					<div class="title">
						<h3><?php echo $objCommon->limitWords($allFlags['f_title'],60)?></h3>
					</div>
					<div class="user-details">
						<div class="img-sec-user">
						<?php
						if($_SESSION['userId'] == $allFlags['user_id']){
							$profileLink		=	SITE_ROOT.'en/profile';
						}else{
							$profileLink		=	SITE_ROOT.'en/profile/'.$allFlags['alias'].'-'.$allFlags['user_id'];
						}
						?>
							<a href="<?php echo $profileLink?>"><img class="img-responsive" src="<?php echo SITE_ROOT.'uploads/profile/'.$userImg?>"></a>
						</div>
						<div class="user_info_name">
							<h3><a href="<?php echo $profileLink?>"><?php echo $objCommon->html2text($allFlags['name'])?></a></h3>
							<p class="map_marker"><a href="<?php echo $profileLink?>"><?php echo $objCommon->html2text($allFlags['f_location'])?><i class="fa fa-map-marker"></i></a></p>
						</div>
					</div>
				</div>
			</div>
			</div>
		<?php
		}
		if (isset($next)){ ?>
		<div id="page-nav">
		<a href="<?php echo SITE_ROOT_EN.'flags/emirates/'.$emirates.'/'.$next?>"></a>
		</div>
		<?php
		}
		}else{?>
            <div class="col-sm-12">
                <div class="nothingfound"></div>
            </div>
		<?php }
		?>  
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.infinitescroll.min.js"></script>
<script type="text/javascript">
	$(function(){
		$(".mylightbox").html5lightbox(); 
     var $container = $('.grid');
     $container.imagesLoaded(function(){
     $container.masonry({
        itemSelector: '.grid-item'
      });
    });
	 $container.infinitescroll({
      navSelector  : '#page-nav',    // selector for the paged navigation 
      nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
      itemSelector : '.grid-item',     // selector for all items you'll retrieve
      loading: {
          finishedMsg: 'No more images to load.',
          img: '<?php echo SITE_ROOT.'images/album_preloader.GIF'; ?>',
        }
      },
      // trigger Masonry as a callback
      function( newElements ) {
        // hide new items while they are loading
        var $newElems = $( newElements ).css({ opacity: 0 });
        // ensure that images load before adding to masonry layout
        $newElems.imagesLoaded(function(){
          // show elems now they're ready
          $newElems.animate({ opacity: 1 });
          $container.masonry( 'appended', $newElems, true );
		  $(".mylightbox").html5lightbox(); 
        });
      }
    );
	});
</script>
<?php
include_once(DIR_ROOT."includes/footer_en.php");
?>
