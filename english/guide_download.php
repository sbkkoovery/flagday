<?php
include_once("../includes/site_root.php");
include_once(DIR_ROOT."includes/header_en.php");
$getDownLoadDetailsGuide					   =	$objSlider->getRowSql("SELECT d_name,d_image FROM ".$LANGUAGE."downloads WHERE d_alias='logo-brand-guide-line'");
?>
<div class="background_div" style="background-image:url('<?php echo SITE_ROOT.'uploads/home_slider/'.$getHomeSlider['slider_image']?>');">
	<div class="white_overlay">
		<div class="container">
			<div class="logo_sec" style="direction:rtl">
            	<div class="padding_it">
                    <h3><?php echo $objCommon->html2text($getDownLoadDetailsGuide['d_name'])?></h3>
                    <?php /*?><div class="row">
                        <div class="col-sm-6">
                            <div class="flag_video">
                                <div class="head_video">
                                    <h3>الشعار المتحرك</h3>
                                </div>
                                <video class="video_flags" width="320" height="240" loop="loop" autoplay="autoplay">
                                      <source src="<?php echo SITE_ROOT?>video/UAE FLAG LOOP V2pole.mp4" type="video/mp4">
                                      Your browser does not support the video tag.
                                </video>
                            </div>
                        </div>
                        <div class="col-sm-6">
                        <div class="flag_logo_section">
                                <div class="title_section_logo">
                                    <h3> الشعار الرسمي </h3>
                                </div>
                                <div class="log_section_flag">
                                    <img class="img-responsive" src="<?php echo SITE_ROOT?>images/flag_logo.png" />
                                </div>
                            </div>
                        </div>
                    </div><?php */?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="red_div">
                                <p>الدليل الإرشادي للهوية الرسمية ليوم العلم لدولة الإمارات العربية المتحدة</p>
                                <p class="eng">FLAG DAY GUIDELINES</p>
                            </div>
                        </div>
                    </div>
                    <div class="guide_lines match_height">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="logo-flag">
                                    <img src="<?php echo SITE_ROOT?>images/flag_guide.png" />
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="guide_info">
                                    <h4>1. الهوية الرئيسية</h4>
                                    <p>هذه هي الهوية الرسمية الرئيسية لإحتفالات بيوم العلم لدولة الإمارات العربية المتحدة. على الجميع الحرص التام في استخدام الهوية الرئيسية بالإضافة إلى العناصر الجرافيكية المساندة لها والتي نعرضها هنا بحذر واحترام بما يتماشى مع دليل إرشادات إستخدام الهوية الإعلامية.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="row">
                            <div class="col-sm-6">
                             <div class="guide_lines match_height">
                            <div class="guide_info">
                                    <h4>3. المساحة الخالية</h4>
                                    <p>إ ن المساحة الخالية للهوية الرئيسية مساوية للطول X من العلم في الشعار.  يجب المحافظة على وضوح وسلامة الهوية الإعلامية وذلك بإبقاء المساحة الخالية كما هي.</p>
                                    <div class="logo-flag">
                                        <img class="img-responsive" src="<?php echo SITE_ROOT?>images/x_y_axis_flag.png" />
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="col-sm-6">
                             <div class="guide_lines">
                                <div class="guide_info">
                                    <h4>2. مكونات الهوية الرئيسية</h4>
                                    <div class="logo-flag upside">
                                        <img class="img-responsive" src="<?php echo SITE_ROOT?>images/logo_instructions.png" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="guide_lines _differ_bg">
                                <div class="guide_info">
                                    <h4>5. الالوان:ية</h4>
                                    <p>اللون هو في غاية الأهمية بالنسبة لإستخدام الهوية الإعلامية.</p>
                                    <div class="logo-flag">
                                        <img class="img-responsive center-block" src="<?php echo SITE_ROOT?>images/color_combinations.png" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="guide_lines">
                                <div class="guide_info">
                                    <h4>4. المساحة الصغرى المستخدمةية</h4>
                                    <p>أصغر حجم يمكنكم استخدامه للهوية الرئيسية هو بعرض 30 مم .</p>
                                    <div class="logo-flag">
                                        <img class="img-responsive center-block" src="<?php echo SITE_ROOT?>images/flag_mm.png" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="guide_lines">
                                <div class="guide_info">
                                    <div class="row">
                                        <div class="col-sm-6">
                                                <div class="logo-flag">
                                                    <img class="img-responsive center-block" src="<?php echo SITE_ROOT?>images/no_flag.png" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                 <h4>4. المساحة الصغرى المستخدمةية</h4>
                                                <p>أصغر حجم يمكنكم استخدامه للهوية الرئيسية هو بعرض 30 مم .</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="download_share text-center">
                	<a href="<?php echo SITE_ROOT.'download.php?file='.$objCommon->html2text($getDownLoadDetailsGuide['d_image'])?>&fileName=logo_guide">Download</a>
                    <a href="#" data-toggle="modal" data-target="#myModal">Share</a>
                </div>
            </div>
		</div>
	</div>
</div>
<div class="modal socialshare fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Share</h4>
      </div>
      <div class="modal-body">
      	 <span class='st_facebook_large' displayText='Facebook'></span>
        <span class='st_twitter_large' displayText='Tweet'></span>
        <span class='st_linkedin_large' displayText='LinkedIn'></span>
        <span class='st_pinterest_large' displayText='Pinterest'></span>
        <span class='st_email_large' displayText='Email'></span>
        <script type="text/javascript" src="<?php echo SITE_ROOT; ?>js/buttons.js"></script>
        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript">stLight.options({publisher: "318b9a8f-b81c-4b92-89da-cb0ad4ccf6d6", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
      </div>
    </div>
  </div>
</div>
<?php
include_once(DIR_ROOT."includes/footer_en.php");
?>