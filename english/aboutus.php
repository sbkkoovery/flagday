<?php
include_once("../includes/site_root.php");
include_once(DIR_ROOT."includes/header_en.php");
include_once(DIR_ROOT."class/".$LANGUAGE."pages.php");
$objCommon				   =	new common();
$objPages				    =	new pages();
$getPage			   		 =	$objPages->getRow("page_id=1");
?>
<div class="background_div" style="background-image:url('<?php echo SITE_ROOT.'uploads/home_slider/'.$getHomeSlider['slider_image']?>');">
<div class="white_overlay">
<div class="container container-alt">
<h1><?php echo $objCommon->html2text($getPage['page_title'])?></h1>
<?php echo $objCommon->html2text($getPage['page_content'])?>
</div>
</div>
</div>
<?php
include_once(DIR_ROOT."includes/footer_en.php");
?>
