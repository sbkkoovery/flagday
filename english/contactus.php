<?php
include_once("../includes/site_root.php");
include_once(DIR_ROOT."includes/header_en.php");
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.validate.js"></script>
<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
<div class="background_div" style="background-image:url('<?php echo SITE_ROOT.'uploads/home_slider/'.$getHomeSlider['slider_image']?>');">
	<div class="white_overlay">
		<div class="container">
			<div class="container_contact">
			<div class="contact_us">
				<div class="contact_head">
				<?php echo $objCommon->displayMsg();?>
					<h3>Contact Us</h3>
					<p>Let us know your suggestions and inquiries</p>
				</div>
				<div class="contact_us_form">
					<form id="submit_contact_us" method="post" enctype="multipart/form-data" action="<?php echo SITE_ROOT?>access/contact_us.php">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									 <label >Name</label>
									<input type="text" class="form-control" name="name" id="name" placeholder="Name">
								</div>
							</div>
                            <div class="col-sm-6">
								<div class="form-group">
									<label >Gender</label>
									<select class="form-control" name="gender" id="gender">
										<option value="">Gender</option>
										<option value="male">Male</option>
										<option value="female">Female</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									 <label >Email Address</label>
									<input type="email" class="form-control"  name="email" id="email" placeholder="Email Address">
								</div>
							</div>
                            <div class="col-sm-6">
								<div class="form-group">
									<label >Phone Number</label>
									<input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									 <label >Message</label>
									<textarea class="form-control" placeholder="Message" name="msg" id="msg"></textarea>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									 <label >Attachments</label>
									<input type="file" id="job_image" name="attachment" >
								</div>
							</div>
							 <div class="col-sm-12">
								<button type="submit" id="contactUs" class="btn btn-default submit_contact">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(e) {
	$("#job_image").filestyle();
	$("#submit_contact_us").validate({
		rules: {
			name: "required",
			msg: "required",
			email:{required:true,email: true}
		},
		messages: {
			name: 'Can\'t be empty',
			msg: 'Can\'t be empty',
			email: {required:'Can\'t be empty',email:'Please enter valid email address'}
		}

	});
});
</script>
<?php
include_once(DIR_ROOT."includes/footer_en.php");
?>