<?php
include_once("../includes/site_root.php");
include_once(DIR_ROOT."includes/header_en.php");
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.validate.js"></script>
<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
<div class="background_div" style="background-image:url('<?php echo SITE_ROOT.'uploads/home_slider/'.$getHomeSlider['slider_image']?>');">
	<div class="white_overlay">
		<div class="container">
			<div class="container_contact">
			<div class="contact_us">
				<div class="contact_head">
				<?php echo $objCommon->displayMsg();?>
					<h3>Change your password</h3>
					<p>Let’s find your account</p>
				</div>
				<div class="contact_us_form">
					<form id="submit_forgot_password" method="post"  action="<?php echo SITE_ROOT?>access/forgot_password_request.php">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									 <label >Username/ Email</label>
									<input type="email" class="form-control" name="forgot_email" id="forgot_email" placeholder="Username/Email">
								</div>
							</div>
						</div>
						<div class="row">
							 <div class="col-sm-12">
							 	<input type="hidden" name="langSwitch" value="en" />
								<button type="submit"  class="btn btn-default submit_contact">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(e) {
	$("#job_image").filestyle();
	$("#submit_forgot_password").validate({
		rules: {
			forgot_email: "required"
		},
		messages: {
			forgot_email: {required:'Can\'t be empty',email:'Please enter valid email address'}
		}

	});
});
</script>
<?php
include_once(DIR_ROOT."includes/footer_en.php");
?>