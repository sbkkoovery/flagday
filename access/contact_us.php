<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/settings.php");
include_once(DIR_ROOT."class/contact_us.php");
include_once(DIR_ROOT.'PHPMailer/class.phpmailer.php');
$objCommon				  =	new common();
$objSettings				=	new settings();
$objContactUs			   =	new contact_us();
$mail             		   = 	new PHPMailer();
$getContactEmail			=	$objSettings->getRow("setting_id=1");
if($_POST['name'] != '' && $_POST['email'] != '' && $_POST['msg'] != ''){
	$name				   =	$_POST['c_name']	=	$objCommon->esc($_POST['name']);
	$email				  =	$_POST['c_email']   =	$objCommon->esc($_POST['email']);
	$msg				    =	$_POST['c_msg']	 =	$objCommon->esc($_POST['msg']);
	$gender				 =	$_POST['c_gender']  =	$objCommon->esc($_POST['gender']);
	$phone				  =	$_POST['c_phone']   =	$objCommon->esc($_POST['phone']);
	$_POST['c_created']	 =	date("Y-m-d H:i:s");
	if($_FILES['attachment']['name']!=""){
		$ext	=	strtolower(pathinfo($_FILES['attachment']['name'], PATHINFO_EXTENSION));
		$validImages	=	array("jpg","jpeg","gif","png","doc","docx","pdf");
		if(in_array($ext,$validImages)){
			$name	=	time();
			if(move_uploaded_file($_FILES['attachment']['tmp_name'],'../uploads/contact_us/'.$name.'.'.$ext))
			{
				$_POST['c_file']	=	$c_file		=	$name.'.'.$ext;
			}
		}
	}
	$objContactUs->insert($_POST);
	$body	=	'<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
						<div style="background-color:#A91620; padding:10px 10%;"><img src="'.SITE_ROOT.'images/logo.png" height="80" /></div>
						<div style="background-color:#f0f0f0; color:#333; padding:10px 10%;">
							<h1>Contact Us</h1>
							<p>Name : '.$name.'</p>
							<p>Email : '.$email.'</p>
							<p>Phone : '.$phone.'</p>
							<p>Gender : '.$gender.'</p>
							<p><b>Message</b></p>
							<p>'.$msg.'</p>
						</div>
						<div style="background-color:#000000; padding:10px 10%; color:#fff; font-size:10px">&copy; All rights are reserved for Flag Day , United Arab Emirates </div>
					</div>';
	$mail->CharSet = 'UTF-8';
	$mail->AddReplyTo($email,$name);
	$mail->SetFrom($email,$name);
	$address = $getContactEmail['setting_link'];
	$mail->AddAddress($address,"UAE Flag Day");
	
	$mail->Subject    = "UAE Flag Day";
	
	$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
	
	$mail->MsgHTML($body);
	if($_FILES['attachment']['name']!=""){
		$mail->AddAttachment('../uploads/contact_us/'.$c_file); // attachment
	}
	
	$mail->Send();
	//echo '<div class="alert alert-success" role="alert">Your query has been sent</div>';
	$objCommon->addMsg('Your query has been sent...',1);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}else{
	//echo '<div class="alert alert-danger" role="alert">Please fill the required fields</div>';
	$objCommon->addMsg('Please fill the required fields...',0);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
?>