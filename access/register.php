<?php
@session_start();
$LANGUAGE				   =	'ar_';
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
$objCommon				  =	new common();
$objUsers				   =	new users();
if($_POST['u_name'] != '' && $_POST['u_email'] != '' && $_POST['u_password'] != '' && $_POST['u_emirates'] != '' && $_POST['u_password']== $_POST['u_con_password']){
	$langSwitch			 =	$_POST['langSwitch'];
	$getEmailAlready		=	$objUsers->getRowSql("SELECT user_id FROM users WHERE email='".$_POST['u_email']."'");
		if($getEmailAlready['user_id'] ==''){
		$u_name				 =	$_POST['name']			=	$objCommon->esc($_POST['u_name']);
		$_POST['alias']		 =	$objCommon->getAlias($u_name);
		$u_email				=	$_POST['email']		   =	$objCommon->esc($_POST['u_email']);
		$u_password			 =	$objCommon->esc($_POST['u_password']);
		$u_emirates			 =	$_POST['emirates']		=	$objCommon->esc($_POST['u_emirates']);
		$_POST['password']	  =	md5($u_password);
		$_POST['status']		=	1;
		$_POST['created_date']  =	date("Y-m-d H:i:s");
		if($_FILES['u_image']['name']!=""){
			$ext				=	strtolower(pathinfo($_FILES['u_image']['name'], PATHINFO_EXTENSION));
			$validImages		=	array("jpg","jpeg","gif","png");
			if(in_array($ext,$validImages)){
				$name		   =	time();
				$objCommon->addIMG($_FILES['u_image'],"../uploads/profile/thumb/",$name,100,100,true);
				$_POST['img_url']=	$objCommon->addIMG($_FILES['u_image'],"../uploads/profile/",$name,380,380,false);
			}
		}
		$objUsers->insert($_POST);
		$lastId				=	$objUsers->insertId();
		$_SESSION['userId']	=	$lastId;
		if($langSwitch=='en'){
			header("location:".SITE_ROOT.'en/profile');
		}else{
			header("location:".SITE_ROOT.'profile');
		}
	}else{
		$_SESSION['register_err']	=	1;
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
	}
}else{
	$_SESSION['register_err']	=	2;
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
?>