<?php
@session_start();
$LANGUAGE				   =	'ar_';
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/flags.php");
include_once(DIR_ROOT."class/flag_more.php");
include(DIR_ROOT."class/resize-class.php");
$objCommon				  =	new common();
$objFlags				   =	new flags();
$objFlagMore				=	new flag_more();
$userId					 =	$_SESSION['userId'];
$path					   =	DIR_ROOT.'uploads/flags_images/';
if($userId !='' && $_POST['photo_location'] != '' && count($_FILES['photo_file']['name']) >0){
	mysql_query("START TRANSACTION");
	$_POST['user_id']  	   =	$userId;
	$photoLocation		  =	$_POST['f_location']	=	$objCommon->esc($_POST['photo_location']);
	$photo_descr			=	$_POST['f_title'] 	   =	$objCommon->esc($_POST['photo_descr']);
	$_POST['f_status']	  =	0;
	$_POST['f_ip']		  =	$objCommon->get_client_ip();
	$_POST['f_created']  	 =	date("Y-m-d H:i:s");
	$objFlags->insert($_POST);
	$_POST['f_id']		  =	$objFlags->insertId();
	if(count($_FILES['photo_file']['name'])>0){
		$todayDate		  =	date('Y-m-d');
		if(!file_exists($path.$todayDate)){
			mkdir($path.$todayDate);
		}
		$pathNew			=	$path.$todayDate.'/';
		if(!file_exists($pathNew."original")){
			mkdir($pathNew."original");
		}
		//---------------------------------------------------------------------------------------------
		$valid_img_formats 	  =	array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
		$valid_video_formats 	=	array("mp4", "mov","mpeg4","avi","wmv","MPEGPS","flv","3gp","WebM","AVI","MP4","FLV");
		$countIter			  =	0;
		foreach($_FILES['photo_file']['tmp_name'] as $i => $tmp_name ){ 
			if($countIter <= $i){
			$ext 				= 	pathinfo($_FILES['photo_file']['name'][$i], PATHINFO_EXTENSION);
			if(in_array($ext,$valid_img_formats)){
				$_POST['fm_type']	   =	1;
				$_POST['fm_status']	 =	1;
				$time		   		   =	time();
				$actual_image_name 	  = 	$time.'_'.$i.".".$ext;
				if(move_uploaded_file($_FILES['photo_file']['tmp_name'][$i], $pathNew.'original/'.$actual_image_name)){
					$resizeObj 		  =	new resize($pathNew.'original/'.$actual_image_name);
					$resizeObj -> resizeImage(1000, 1000, 'auto');
					$resizeObj -> saveImage($pathNew.$actual_image_name, 100);
					$_POST['fm_url']	=	$todayDate."/".$actual_image_name;
					$objFlagMore->insert($_POST);
				}
			}else if(in_array($ext,$valid_video_formats)){
				if($_FILES['photo_file']['size'][$i] < 104857600){
					$pathNew			=	$path.$todayDate.'/';
					if(!file_exists($pathNew."thumb")){
						mkdir($pathNew."thumb");
					}
					$pathThumb			  =	$pathNew.'thumb/';
					$time		   		   =	time();
					$_POST['fm_type']	   =	2;
					$_POST['fm_status']	 =	1;
					$actual_vid_name 		= 	$time."_".$i.".".$ext;
					$newVidName		 	 =	$time."_".$i.".mp4";
					//shell_exec('/usr/local/bin/ffmpeg -i '.$_FILES['photo_file']['tmp_name'][$i].' -strict -2 '.$pathNew.$newVidName);
					//shell_exec('/usr/local/bin/ffmpeg -itsoffset -4  -i '.$_FILES['photo_file']['tmp_name'][$i].' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 320x240 '.$pathThumb.$time.'_'.$i.'.jpg');
					//exec('c:/xampp/htdocs/flagday/ffmpeg/bin/ffmpeg -i '.$_FILES['photo_file']['tmp_name'][$i].' '.$pathNew.$newVidName);
					//exec('c:/xampp/htdocs/flagday/ffmpeg/bin/ffmpeg -itsoffset -4  -i '.$_FILES['photo_file']['tmp_name'][$i].' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 320x240 '.$pathThumb.$time.'_'.$i.'.jpg');
					exec('C:/wamp/www/flagday/ffmpeg/bin/ffmpeg -i '.$_FILES['photo_file']['tmp_name'][$i].' '.$pathNew.$newVidName);
					exec('C:/wamp/www/flagday/ffmpeg/bin/ffmpeg -itsoffset -4  -i '.$_FILES['photo_file']['tmp_name'][$i].' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 320x240 '.$pathThumb.$time.'_'.$i.'.jpg');
					$videofile		  	  =	$pathNew.$newVidName;
					$_POST['fm_url']	 	=	$todayDate.'/'.$newVidName;
					$_POST['fm_thumb']	  =	$todayDate.'/'.$time.'_'.$i.'.jpg';
					$objFlagMore->insert($_POST);
				}
			}
			$countIter++;
			}
		}
	}
	echo '<input type="hidden" id="uploadStatus" value="1" />';
	//------------------------------------------------------------------------------------------------
	mysql_query("COMMIT");
}else{
	echo '<div class="alert alert-danger" role="alert">Please fill the required fields</div>';
	echo '<input type="hidden" id="uploadStatus" value="1" />';
}
?>