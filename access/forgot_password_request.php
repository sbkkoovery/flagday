<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/forgot_password.php");
include_once(DIR_ROOT."class/users.php");
include_once(DIR_ROOT."class/settings.php");
include_once(DIR_ROOT.'PHPMailer/class.phpmailer.php');
$objCommon				  =	new common();
$objUser					=	new users();
$objForgot				  =	new forgot_password();
$mail             		   = 	new PHPMailer();
$objSettings				=	new settings();
$getContactEmail			=	$objSettings->getRow("setting_id=1");
$langSwitch				 =	$_POST['langSwitch'];
if($langSwitch=='en'){
	$redLang				=	SITE_ROOT_EN;
}
else{
	$redLang				=	SITE_ROOT;
}
if($_POST['forgot_email'] != ''){
	$forgot_email		   =	$_POST['forgot_email'];
	$getuserDetails		 =	$objUser->getRow("email='".$forgot_email."' and status=1");
	if($getuserDetails['user_id']){
		$encrpStr	   	   =	$objCommon->randStrGen('10').'-'.$getuserDetails['user_id'];
		$_POST['user_id']   =	$getuserDetails['user_id'];
		$_POST['fp_string'] =	$encrpStr;
		$objForgot->delete('user_id='.$getuserDetails['user_id']);
		$objForgot->insert($_POST);
		$body			   =	'<table style="width:100%;border-collapse:collapse"><tbody><tr><td style="font:14px/1.4285714 Arial,sans-serif;padding:10px 10px 0;background:#f5f5f5">
            <table style="width:100%;border-collapse:collapse"><tbody><tr><td style="font:14px/1.4285714 Arial,sans-serif;padding:0;background-color:#ffffff;border-radius:5px">
    <div style="border:1px solid #cccccc;border-radius:5px;padding:20px">
      <table style="width:100%;border-collapse:collapse"><tbody><tr><td style="font:14px/1.4285714 Arial,sans-serif;padding:0">
              <p style="margin-bottom:0;margin-top:0">
                
                  You are receiving this email because you requested a password reset
                  for the user <strong>'.$getuserDetails['name'].'</strong>.
                
              </p>
            </td>
          </tr><tr><td style="font:14px/1.4285714 Arial,sans-serif;padding:15px 0 0">
  <table style="width:auto;border-collapse:collapse"><tbody><tr><td style="font:14px/1.4285714 Arial,sans-serif;padding:0">
          <div style="border:1px solid #486582;border-radius:3px">
            <table style="width:auto;border-collapse:collapse"><tbody><tr><td style="font:14px/1.4285714 Arial,sans-serif;padding:4px 10px;background-color:#3068a2">
                  
              <a href="'.$redLang.'forgot-password-request/'.$encrpStr.'" style="color:white;text-decoration:none;font-weight:bold" target="_blank">Reset password</a>
            
                </td>
              </tr></tbody></table></div>
        </td>
      </tr></tbody></table></td>

          </tr></tbody></table></div>
  </td>
</tr><tr><td style="font:14px/1.4285714 Arial,sans-serif;padding:20px 0;color:#707070">
  <table style="width:100%;border-collapse:collapse"><tbody><tr><td style="font:14px/1.4285714 Arial,sans-serif;padding:0">
          
          
        </td>
        <td style="font:14px/1.4285714 Arial,sans-serif;padding:0">
          
        </td>
        <td style="font:14px/1.4285714 Arial,sans-serif;padding:0;text-align:right;width:100px">
          <a href="<?php echo SITE_ROOT?>" style="color:#3572b0;text-decoration:none" target="_blank">
            <img  src="'.SITE_ROOT.'images/logo.png"  class="CToWUd" /></a>
        </td>
      </tr></tbody></table></td>

                </tr></tbody></table></td>
        </tr></tbody></table>';
	$mail->CharSet = 'UTF-8';
	$mail->AddReplyTo($getContactEmail['setting_link'],'UAE Flagday');
	$mail->SetFrom($getContactEmail['setting_link'],'UAE Flagday');
	$address = $getContactEmail['setting_link'];
	$mail->AddAddress($getuserDetails['email'],$getuserDetails['name']);
	
	$mail->Subject    = "Here's the link to reset your password";
	
	$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
	
	$mail->MsgHTML($body);
	$mail->Send();
	//echo '<div class="alert alert-success" role="alert">Your query has been sent</div>';
	$objCommon->addMsg("We've sent you a link to change your password...",1);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
	}else{
		$objCommon->addMsg('Invalid username/email address',0);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
	}
}else{
	//echo '<div class="alert alert-danger" role="alert">Please fill the required fields</div>';
	$objCommon->addMsg('Please fill the required fields...',0);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
?>