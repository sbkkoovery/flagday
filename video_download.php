<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
$getDownLoadDetails					=	$objSlider->getRowSql("SELECT d_name,d_image FROM ".$LANGUAGE."downloads WHERE d_alias='animated-logo'");
?>
<div class="background_div" style="background-image:url('<?php echo SITE_ROOT.'uploads/home_slider/'.$getHomeSlider['slider_image']?>');">
	<div class="white_overlay">
		<div class="container">
			<div class="logo_sec">
                <h3><?php echo $objCommon->html2text($getDownLoadDetails['d_name'])?></h3>
                    <video class="video_flags" width="320" height="240" loop="loop" autoplay="autoplay">
                        <source src="<?php echo SITE_ROOT.'uploads/downloads/'.$objCommon->html2text($getDownLoadDetails['d_image'])?>" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                <div class="download_share text-center">
                	<a href="<?php echo SITE_ROOT.'download.php?file='.$objCommon->html2text($getDownLoadDetails['d_image'])?>&fileName=animated_logo" download >تحميل</a>
                    <a href="javascript:;" data-toggle="modal" data-target="#myModal">مشاركة</a>
                </div>
            </div>
		</div>
	</div>
</div>
<div class="modal socialshare fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">مشاركة</h4>
      </div>
      <div class="modal-body">
      	 <span class='st_facebook_large' displayText='Facebook'></span>
        <span class='st_twitter_large' displayText='Tweet'></span>
        <span class='st_linkedin_large' displayText='LinkedIn'></span>
        <span class='st_pinterest_large' displayText='Pinterest'></span>
        <span class='st_email_large' displayText='Email'></span>
        <script type="text/javascript" src="<?php echo SITE_ROOT; ?>js/buttons.js"></script>
        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript">stLight.options({publisher: "318b9a8f-b81c-4b92-89da-cb0ad4ccf6d6", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
      </div>
    </div>
  </div>
</div>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>