<?php
	function html2text($s){
		$s				= html_entity_decode($s);
		$s				= str_replace("\\r\\n","<br/>",$s);
		$s				= str_replace("\r\n","<br/>",$s);
		$s				= str_replace("\\n","<br/>",$s);
		$s    			= str_replace("'","&#39;",$s);
		$s				= nl2br($s,false);
		$s				= stripslashes($s);
		return $s;
	}
	$str	=	'You&#39;ve probably heard all about iShoot. Written by a programmer at Sun Microsystems in his spare time, this Worms-style artillery shooter blasted to the top of the App Store charts earlier this year and stayed there for weeks earning its creator enough money to pack in his day job and become a professional developer.\\r\\n\\r\\nYou may also have seen the news this week about nine-year-old programming prodigy Lim Ding Wen who has developed his own simple painting app for the iPhone. Doodle Kids is doing reasonable business, attracting 4,000 downloads in less than a fortnight and gaining its author international news coverage.\\r\\n\\r\\nThere&#39;s no question about it, iPhone has become the people&#39;s platform. While the PC maintains a healthy indie development scene, Apple&#39;s handset, with its low barriers to entry and seamless consumer purchasing system, is the real rags-to-riches machine. In practically a month, you can develop an application that will be available to a global market of enthusiastic downloaders.\\r\\n\\r\\nPublishers? Distribution partners? Specialist development hardware? None of it is necessary. In the same way that cheap, accessible digital technology has brought film and music making to the masses, iPhone seems to have sliced through the painstaking game production pipeline. Everyone can be Will Wright now.\\r\\n\\r\\nOr can they? How exactly do you go about creating an iPhone title?\\r\\n\\r\\nI&#39;ve spoken to iShoot coder Ethan Nicholas and two British studios involved in iPhone development - FluidPixel, responsible for fun Lemmings-style platformer, KamiCrazy and Connect2Media&#39;s Manchester team, currently finishing off the hugely promising, Go! Go! Rescue Squad. Here are their tips to any burgeoning app stars&hellip;';
	echo html2text($str);
?>