
<div class="widget">
<?php /*?><?php if($websiteSeting['status']){?>
<div class="examination"><h1 style="padding-top:0px;"><a href="<?php echo SITE_ROOT; ?>examination/">دراسات</a></h1></div>
<?php }else{?>
<div class="examination"><h1>تقويم</h1></div>
<?php }?><?php */?>
<div class="examination"><h1>التقويم</h1></div>
<link rel="stylesheet" href="<?php echo SITE_ROOT; ?>css/eventCalendar.css">
<link rel="stylesheet" href="<?php echo SITE_ROOT; ?>css/eventCalendar_theme_responsive.css">

<div class="widget_white calender">
<div class="event_calender">
	<div data-current-year="2015" data-current-month="3" class="eventCalendar-wrap" id="eventCalendarHumanDate" style="margin-bottom:0px;">
        <div style="height: 258px;" class="eventsCalendar-slider">
          <div style="width: 466px;" class="eventsCalendar-monthWrap currentMonth">
            <div class="eventsCalendar-currentTitle"><a href="#" class="monthTitle"><?php echo date("F Y"); ?></a></div>
            <ul class="eventsCalendar-daysList showAsWeek showDayNames">
              <li class="eventsCalendar-day-header">Mon</li>
              <li class="eventsCalendar-day-header">Tue</li>
              <li class="eventsCalendar-day-header">Wed</li>
              <li class="eventsCalendar-day-header">Thu</li>
              <li class="eventsCalendar-day-header">Fri</li>
              <li class="eventsCalendar-day-header">Sat</li>
              <li class="eventsCalendar-day-header">Sun</li>
              <?php 
			  $firstDayCount	=	date("N",strtotime(date("Y-m-01")));
			  for($i=1;$i<$firstDayCount;$i++){
			  ?>
              <li class="eventsCalendar-day empty"></li>
              <?php }?>
              <?php
			  $countDays	=	cal_days_in_month(CAL_GREGORIAN, date("n"), date("Y"));
              for($i=1;$i<=$countDays;$i++){
				  $eventDate	=	date("Y-m-").sprintf('%02d', $i);
				  $eventCount	=	$objEvents->countRows("SELECT desc_id FROM ar_event_description WHERE DATE(start_date)='$eventDate'");
			  ?>
              <li id="dayList_<?php echo $i; ?>" rel="<?php echo $i; ?>" class="eventsCalendar-day <?php echo ($eventCount>0)?'dayWithEvents':''; ?> <?php echo ($i==date('j'))?'today':''; ?>"><a href="#"><?php echo $i; ?></a></li>
              <?php }?>
            </ul>
          </div>
          <a href="#" class="arrow prev"><span>prev</span></a><a href="#" class="arrow next"><span>next</span></a></div>
          <div class="clearfix"></div>
      </div>
        <div class="calender_btns">
            <!--<a href="#" class="event_btn">Upcoming Events</a>
            <a href="#" class="event_btn">Past Events</a>-->
            <a href="<?php echo SITE_ROOT; ?>events.php" class="event_btn">جميع الفعاليات <i class="fa fa-chevron-left"></i></a>
        </div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$("#eventCalendarHumanDate").eventCalendar({
		eventsjson: '<?php echo SITE_ROOT; ?>ajax/ar_eventJson.php',
		jsonDateFormat: 'human'  // 'YYYY-MM-DD HH:MM:SS'
	});
});
$(".event_calender").on('mouseenter','.dayWithEvents', function(e) {
	
	var currPos		=	$(this);
	var  monthYear	=	$(this).parent().parent().children(".eventsCalendar-currentTitle").text();
	var	dayDate	=	$(this).children("a").text();
	tmrMenu = setTimeout(function() {
		$.get("<?php echo SITE_ROOT; ?>ajax/ar_getEvent.php",{month_year:monthYear,day_date:dayDate},function(data){
			if(data!=""){
				currPos.prepend(data);
				$(".eventCalanderDetails").fadeIn(200);
			}
		});
	}, 150);
})
$(".event_calender").on('mouseleave','.dayWithEvents', function(e) {
		clearTimeout(tmrMenu); // clears hiding timer
		$(".eventCalanderDetails").fadeOut(200,function(){
			$( this ).remove();
		});
});
</script>
<script src="<?php echo SITE_ROOT; ?>js/moment.js" type="text/javascript"></script> 
<!--script src="js/jquery.eventCalendar.js" type="text/javascript"></script--> 
<script src="<?php echo SITE_ROOT; ?>js/jquery.js" type="text/javascript"></script> 
</div>
<div class="widget">
<div class="widget_white twitter_follow">
 <a class="twitter-timeline" href="https://twitter.com/Watani" data-widget-id="586763542080040960">Tweets by @Watani</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script> 
</div>
</div>
<?php if($pageName=='index.php'){
include_once(DIR_ROOT."class/ar_contribution.php");
$objContribution							=	new contribution();
$getAllContributes						  =	$objContribution->getAll("c_status=1","c_date desc limit 0,3");
if(count($getAllContributes)>0){
?>
<div class="widget">
<div class="widget_white visitors">
<h1 class="heading"><span><i class="fa fa-comments-o"></i>مشاركات الزوار </span></h1>
<?php
foreach($getAllContributes as $allContributes){
?>
<div class="visitor_box">
  <h5><?php echo $objCommon->html2text($allContributes['c_name']);?></h5>
  <p><?php echo $objCommon->limitWords($allContributes['c_message'],200);?> ... <a href="<?php echo SITE_ROOT.'contribution.php'?>">اقرأ المزيد</a></p>
</div>
<?php
}
?>
</div>
</div>
<?php
}
?>
<?php }else if($pageName!='volunteers.php' && $pageName!='volunteers_second.php' && $pageName!='volunteers_third.php'){
	 $_SESSION['step']=0;
	$bookList	=	$objBooks->getAll("status=1");
if(count($bookList)>0){
	 ?>
<div class="widget">
    <div class="widget_white">
      <div class="our_publication">
        <h1 class="heading"><span><i class="fa fa-file-text"></i>اصدراتنا</span></h1>
        <div class="new">
        <?php
		foreach($bookList as $books){
		?>
          <div class="item">
            <div class="books arabicDirection">
              <div class="pic"><a href="<?php echo SITE_ROOT.'bookdetails.php?bookId='.$books['book_id']; ?>"><img src="<?php echo SITE_ROOT; ?>images/books/<?php echo $books['book_icon']; ?>" alt="books" /></a></div>
              <p><?php echo $objCommon->limitWords($books['book_title'],30); ?></p>
              <div class="download"><a href="<?php echo SITE_ROOT.'read-book.php?bookId='.$books['book_id']; ?>">تحميل</a></div>
            </div>
          </div>
        <?php }?>
        </div>
      </div>
    </div>
</div>
<?php }}?>