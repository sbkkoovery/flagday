<?php
$getSocialLinks				=	$objSlider->getRowSql("SELECT * FROM social_links WHERE sl_id=1");
?>
 <div class="footer">
        	<div class="container">
            	<div class="row">
                	<div class="col-sm-5">
                    	<div class="copy_rights">
                            <p>© <?php echo date("Y")?> - All rights are reserved for Flag Day , United Arab Emirates </p>
                            <p class="website_support">Website is supported by  Watani Al Emarat Foundation</p>
                        </div>
                    </div>
                    <div class="col-sm-7">
                    	<div class="footer_nav">
                        	<ul>
                            	<li><a href="<?php echo SITE_ROOT_EN?>about-us">About Flag Day   </a></li>
                                <li><a href="<?php echo SITE_ROOT_EN?>Flags">Participate in Flag Day</a></li>
                                <li><a href="<?php echo SITE_ROOT_EN?>contact-us">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="footer_social_connects pull-right">
                        	<a href="<?php echo $objCommon->html2text($getSocialLinks['sl_facebook'])?>" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="<?php echo $objCommon->html2text($getSocialLinks['sl_instagram'])?>" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a href="<?php echo $objCommon->html2text($getSocialLinks['sl_twitter'])?>" target="_blank"><i class="fa fa-twitter"></i></a>>
                        </div>
						<div class="mobile_app pull-right">
							 <a href="https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=1053876973&mt=8" target="_blank"><img src="<?php echo SITE_ROOT?>images/app_store.png" /></a>
                            <a href="https://play.google.com/store/apps/details?id=com.Flag.flagday&hl=en" target="_blank"><img src="<?php echo SITE_ROOT?>images/google_play.png" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div><!-----main-container---->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script type="text/javascript" src="<?php echo SITE_ROOT?>js/perfect-scrollbar.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/perfect-scrollbar.jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.matchHeight-min.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/bootstrap-filestyle.min.js"></script>
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>

<script type="text/javascript">
	$(document).ready(function(e) {
		var windwidth	=	$(window).width();
			$(".nav_section_slides").perfectScrollbar();
});	
</script>
  </body>
</html>