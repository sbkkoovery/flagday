<?php
@session_start();
ob_start();
$LANGUAGE					=	'';
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/".$LANGUAGE."slider.php");
$objCommon				   =	new common();
$objSlider				   =	new slider();
$getHomeSlider			   =	$objSlider->getRow();
$getDownloadMenu			 =	$objSlider->listQuery("SELECT d_name,d_image,d_alias FROM ".$LANGUAGE."downloads ORDER BY d_id");
if($LANGUAGE != 'ar_'){
	$reqUrl				  =	$_SERVER['REDIRECT_URL'];
	if($_SERVER['HTTP_HOST']=='localhost'){
		$newReqUrl		   =	str_replace("/flagday/en/","",$reqUrl);
	}else{
		//$newReqUrl		   =	str_replace("/workstation/demo/flagday/live/en/","",$reqUrl);
		$newReqUrl		   =	str_replace("/flagday/en/","",$reqUrl);
	}
}
$pageName = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Flagday | Welcome</title>
	<!-- Bootstrap -->
	<link rel="shortcut icon" href="<?php echo SITE_ROOT?>images/fevicon.png">
	<link type="text/css" rel="stylesheet" href="<?php echo SITE_ROOT?>css/bootstrap.min.css" >
	<link type="text/css" rel="stylesheet" href="<?php echo SITE_ROOT?>css/en_style.css" >
	<link type="text/css" rel="stylesheet" href="<?php echo SITE_ROOT?>css/en_media.css" >
	<link href='https://fonts.googleapis.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>css/perfect-scrollbar.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
	<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery-1.11.3.min .js"></script>
	<script type="text/javascript" src="<?php echo SITE_ROOT?>js/bootstrap.min.js"></script>
	<?php
	  if($pageName == 'emirates_flags.php'){
	 ?>
	 <script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery-1.7.1.min.js"></script>
	<?php }?>
	<script type="text/javascript" src="<?php echo SITE_ROOT?>js/html5lightbox.js"></script>
	<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo SITE_ROOT?>js/script.js"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script type="text/javascript" src="<?php echo SITE_ROOT?>js/respond.js"></script>
	  <script type="text/javascript" src="<?php echo SITE_ROOT?>js/html5shiv.js"></script>
	<![endif]-->
</head>
  <body>
	<div class="main-container">
    	<div class="navigation_bar">
          <div class="logo_section">
		   <div class="logo">
            	<a href="<?php echo SITE_ROOT_EN?>"><img class="img-responsive center-block" src="<?php echo SITE_ROOT?>images/logo.png" /></a>
            </div>
            <div class="language_switch">
             <a href="<?php echo SITE_ROOT.$newReqUrl?>">عربي</a>
            </div>
          </div><!----logo-section--->
           <div class="navigation_menu">
           	<span class="hidden-lg pull-left _close"><i class="fa fa-close"></i></span>
               <ul>
                <li data-li="nav"><a href="<?php echo SITE_ROOT_EN?>about-us">About Flag Day</a></li>
                <li data-li="nav"><a href="javascript:;" >Download Center</a>
                	<ul>
						<?php
						foreach($getDownloadMenu as $allDownloadMenu){
						?>
							<li><a href="<?php echo SITE_ROOT_EN?>download/<?php echo $objCommon->html2text($allDownloadMenu['d_alias'])?>"><?php echo $objCommon->html2text($allDownloadMenu['d_name'])?></a></li>
						<?php
						}
						 ?>
                    </ul>
                </li>
                <li data-li="nav"><a href="<?php echo SITE_ROOT_EN?>flags" >Participate in Flag Day</a> </li>
                <li data-li="nav"><a href="<?php echo SITE_ROOT_EN?>contact-us"  class="no-border">Contact Us</a></li>
              </ul>
          </div>
          <div class="mobil_menu hidden-lg"><i class="fa fa-bars"></i></div>
          <div class="clearfix"></div>
        </div><!---navigation_bar---->