<style type="text/css">
ul {
	list-style: none;
	margin: 0;
	padding: 0;
}
.source, .source2 {
	width: 350px;
	margin: 0 auto;
	border: 1px solid #e0e0e0;
	background: white;
	display: none;
	white-space: pre;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
}
.source2 {
	width: 450px;
}
.example2 {
	width: 100%;
	padding: 15px;
	background: white;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	background-color:#78AFD6;
}
.example2 .tweet {
	overflow: hidden;
}
.example2 ul {
	width: 99999px;
	height: 95px;
}
.example2 ul li {
	width: 100%;
	height: 95px;
	padding: 0 10px 0 0;
	margin: 0 10px 0 0;
	color:#fff;
}
.tweet ul li a{
	color:#000 !important;
}
</style>
<script type="text/javascript" src="<?php echo SITE_ROOT?>tweetie.js"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=773825196027836";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="container">
        	<div class="social_account_section">
                <div class="row">
                    <div class="col-sm-12 col-lg-sp-9">
                        <div class="twitter_tweets">
                            <div class="example2">
								<div class="tweet"></div>
							</div>
                        </div>
                        <div class="instagram_widget">
                            <iframe src="http://snapwidget.com/sc/?u=d2F0YW5pX2FlfGlufDI1N3wzfDN8fG5vfDV8bm9uZXxvblN0YXJ0fHllc3xubw==&ve=310315" title="Instagram Widget" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; height:136px; margin:0px;"></iframe>
                        </div>
                    </div>
                    <div class="col-sm-5 col-lg-sp-3">
                        <div class="facebook_updates">
                        	<div class="fb-page" data-href="https://www.facebook.com/watani.ae?fref=ts" data-width="293" data-height="278" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/watani.ae?fref=ts"><a href="https://www.facebook.com/watani.ae?fref=ts">WATANI</a></blockquote></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script class="source2" type="text/javascript">
	$('.example2 .tweet').twittie({
		username: 'Watani',
		list: 'c-oo-l-e-s-t-nerds-i-know',
		dateFormat: '%b. %d, %Y',
		template: '<strong class="date">{{date}}</strong> - {{screen_name}} {{tweet}}',
		count: 10
	}, function () {
		setInterval(function() {
			var item = $('.example2 .tweet ul').find('li:first');

			item.animate( {marginLeft: '-220px', 'opacity': '0'}, 50000, function() {
				$(this).detach().appendTo('.example2 .tweet ul').removeAttr('style');
			});
		}, 5000);
	});
</script>
