<?php 
include_once(DIR_ROOT."class/partners.php");
$objPartners	=	new partners();
$partnerList	=	$objPartners->getAll("status=1");
?>
<div class="partners_section">
  <h1 class="heading"><span>شركاؤنا</span></h1>
  <div class="partners_slider">
    <?php
    foreach($partnerList as $partners){?>
    <div class="item"><img src="<?php echo SITE_ROOT; ?>images/partners/<?php echo $partners['partner_logo']; ?>" alt="<?php echo $partners['partner_name']; ?>" /></div>
    <?php }?>
  </div>
  <div class="customNavigation"> <a id="prev"><i class="fa fa-chevron-left">&nbsp;</i></a> <a id="next"><i class="fa fa-chevron-right">&nbsp;</i></a> </div>
</div>