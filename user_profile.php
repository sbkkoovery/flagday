<?php
@session_start();
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."class/users.php");
$objUsers				   =	new users();
if($_GET['user_id'] ==''){
	header("location:".SITE_ROOT."flags");
	exit;
}
$user_id					=	$objCommon->esc($_GET['user_id']);
$adIdExpl				   =	explode("-",$user_id);
$getUserId				  =	end($adIdExpl);
$getAdAliasArr			  =	array_slice($adIdExpl, 0, -1);
$getUserAlias			   =	implode("-",$getAdAliasArr);
$getUserDetials			 =	$objUsers->getRowSql("SELECT user.*,emirates.e_name_ar  FROM users AS user LEFT JOIN emirates ON user.emirates = emirates.e_id  WHERE  user.user_id=".$getUserId." AND user.alias ='".$getUserAlias."'");
if($getUserDetials['user_id']){
	$userImg				=	($getUserDetials['img_url'] !='')?$getUserDetials['img_url']:'no-image.jpg';
	$getMyFlags			 =	$objUsers->listQuery("SELECT more.*,flag.f_title,flag.f_location,flag.user_id,flag.f_created
														 FROM flag_more  AS more 
														 LEFT JOIN flags AS flag ON more.f_id = flag.f_id
														 WHERE flag.f_status=1 AND more.fm_status=1 AND flag.user_id = ".$getUserDetials['user_id']." ORDER BY flag.f_created desc");
}else{
	header("location:".SITE_ROOT."flags");
	exit;
}
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/masonry.pkgd.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/imagesloaded.pkgd.js"></script>
<div class="background_div " style="background-image:url('<?php echo SITE_ROOT.'uploads/home_slider/'.$getHomeSlider['slider_image']?>');">
	<div class="white_overlay">
		<div class="container">
			<div class="profile_section">
				<div class="top_profile userprof">
					<div class="row">
						<div class="col-sm-6 col-lg-7">
							<div class="row uploadWidget"><div class="col-lg-12 hidden-sm hidden-xs hidden-md"></div></div>
						</div>
						<div class="col-sm-6 col-lg-5">
							<div class="profile_container">
								<div class="row">
									<div class="col-sm-9">
										<div class="user-info_profile">
											<h3><?php echo $objCommon->html2text($getUserDetials['name'])?></h3>
											<p><?php echo $objCommon->html2text($getUserDetials['e_name_ar'])?>, الإمارات العربية المتحدة</p>
											<p><?php echo $objCommon->html2text($getUserDetials['email'])?></p>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="profile_image">
											<img class="img-responsive" src="<?php echo SITE_ROOT.'uploads/profile/'.$userImg?>">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="red_div profile_red">
					<h3>رواق</h3>
				</div>
				<div class="photo_gallery">
					<div class="row grid">
						<?php
						if(count($getMyFlags) >0){
							foreach($getMyFlags as $allMyFlags){
								$uploadType		 =	$allMyFlags['fm_type'];
								if($uploadType ==1){
									$flag_images	=	$allMyFlags['fm_url'];
								}else if($uploadType ==2){
									$flag_url	   =	$allMyFlags['fm_url'];
									$flag_images	=	$objCommon->getThumb($allMyFlags['fm_thumb']);
								}
						?>
						<div class="col-sm-2 col-lg-3 grid-item">
							<div class="item-sec">
								<div class="img-sec-masnory">
									<?php
									if($uploadType ==1){
										?>
									<a href="<?php echo SITE_ROOT.'uploads/flags_images/'.$flag_images?>" class="mylightbox"   title="<?php echo $objCommon->html2text($allMyFlags['f_title'])?>"><img class="img-responsive" src="<?php echo SITE_ROOT.'uploads/flags_images/'.$flag_images?>"></a>
									<?php
									}else if($uploadType ==2){
									?>
									<a href="JavaScript:html5Lightbox.showLightbox(2, '<?php echo SITE_ROOT?>uploads/flags_images/<?php echo $flag_url?>', '<?php echo $objCommon->html2text($allMyFlags['f_title'])?>', 800, 450, '<?php echo SITE_ROOT?>uploads/flags_images/<?php echo $flag_url?>');"><img class="img-responsive" src="<?php echo SITE_ROOT.'uploads/flags_images/'.$flag_images?>"><span class="videoIcon"><i class="fa fa-video-camera"></i></span></a>
									<?php
									}
									?>
								</div>
								<div class="title_info">
									<div class="title">
										<h3><?php echo $objCommon->limitWords($allMyFlags['f_title'],60)?></h3>
									</div>
									<div class="user-details">
										<div class="img-sec-user">
											 <img class="img-responsive" src="<?php echo SITE_ROOT.'uploads/profile/'.$userImg?>">
										</div>
										<div class="user_info_name">
											<h3><?php echo $objCommon->html2text($getUserDetails['name'])?></h3>
											<p class="map_marker"><?php echo $objCommon->html2text($allMyFlags['f_location'])?><i class="fa fa-map-marker"></i></p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php
							}
						}else{?>
                        <div class="col-sm-12">
                        	<div class="nothingfound">
                                
                            </div>
                        </div>
					<?php }
						?>
				
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
		jQuery(".mylightbox").html5lightbox();
	});
	
	$(function(){
     var $container = $('.grid');
     $container.imagesLoaded(function(){
     $container.masonry({
        itemSelector: '.grid-item'
      });
    });
	});

</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
