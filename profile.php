<?php
@session_start();
ob_start();
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
if($_SESSION['userId'] ==''){
	header("location:".SITE_ROOT."flags");
	exit;
}
include_once(DIR_ROOT."class/users.php");
include_once(DIR_ROOT."class/emirates.php");
$objEmirates				=	new emirates();
$objUsers				   =	new users();
$getUserDetials			 =	$objUsers->getRowSql("SELECT user.*,emirates.e_name_ar  FROM users AS user LEFT JOIN emirates ON user.emirates = emirates.e_id  WHERE  user.user_id=".$_SESSION['userId']);
$getAllEmirates			 =	$objEmirates->getAll("","e_id");
if($getUserDetials['user_id']){
	$userImg				=	($getUserDetials['img_url'] !='')?$getUserDetials['img_url']:'no-image.jpg';
}
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.validate.js"></script>
<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
 <script src="<?php echo SITE_ROOT?>js/jquery.geocomplete.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/masonry.pkgd.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/imagesloaded.pkgd.js"></script>
<script type="text/javascript">
	function PreviewImage(no) {
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("uploadImage"+no).files[0]);
		oFReader.onload = function (oFREvent) {
			document.getElementById("uploadPreview"+no).src = oFREvent.target.result;
		};
	}
</script>
<div class="background_div " style="background-image:url('<?php echo SITE_ROOT.'uploads/home_slider/'.$getHomeSlider['slider_image']?>');">
	<div class="white_overlay">
		<div class="container">
			<div class="profile_section">
				<div class="top_profile">
					<div class="row">
					<form action="<?php echo SITE_ROOT.'access/upload_photos.php'?>" id="uploadPhotos" method="post" enctype="multipart/form-data">
						<div class="col-sm-12 col-lg-7">
							<div id="preview_uploads"></div>
							<div class="row uploadWidget">
								<div class="col-sm-6 col-lg-7">
									<div class="agree_terms">
										<div class="form-group locations">
											<input type="text" class="form-control" name="photo_location" placeholder="يرجى إدخال مكانك" onblur="valideUpload();"  onFocus="geolocate()" autocomplete="off"  id="locality"  /><span class="noifyError" data-container="body" data-toggle="popover" data-placement="top" data-content="1. Select a file to upload 2. Select a place. 3.Place must be in the order 'place - emirates - United Arab Emirates'"><img src="<?php echo SITE_ROOT ?>images/info.png" /></span>
										</div>
										<div class="form-group locations">
											<textarea class="form-control" placeholder="الوصف" name="photo_descr" id="photo_descr"></textarea>
										</div>
										<p>يرجى تحميل الصور و مقاطع الفيديو ذات الصلة  اليوم العلم</p>
									</div>
								</div>
								<div class="col-xs-9 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-lg-4 col-lg-offset-0">
									<div class="profile_container no-border center-block">
										<div class="drag-upload custom-upload ">
											<img class="left-shadow" src="images/shadow-left.png">
											<img class="right-shadow" src="images/shadow-right.png">
											<img class="cloud center-block" src="images/cloud.png">
											<div class="upload-title text-center">
												<p>انقر هنا لتحميل صور / فيديو</p>
											   <input onchange="valideUpload();" type="file" class="file_browse"  name="photo_file[]" multiple="multiple" id="photo_file">
												<button class="btn btn-default upoad-btn" disabled="disabled">تحميل</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="upload_preloader"><img class="img-responsive center-block" src="<?php echo SITE_ROOT?>images/preloader.gif" width="100"/></div>
						</div>
					</form>
						<div class="col-sm-6 col-sm-offset-3 col-lg-offset-0 col-lg-5">
							<div class="profile_container">
								<div class="row">
									<div class="col-sm-9">
										<div class="user-info_profile">
											<h3><?php echo $objCommon->html2text($getUserDetials['name'])?></h3>
											<p class="ar_font"><?php echo $objCommon->html2text($getUserDetials['e_name_ar'])?>, الإمارات العربية المتحدة</p>
											<p><?php echo $objCommon->html2text($getUserDetials['email'])?></p>
											<p class="ar_font">
												<a href="javascript:;" class="Edit logout" data-toggle="modal" data-target="#editModal">تعديل ملفي الشخصي</a>&nbsp;&nbsp;
                                            	<a href="<?php echo SITE_ROOT?>access/logout.php?lang=ar" class="logout">خروج</a>
                                            </p>
										</div>
									</div>
									<div class="col-xs-4 col-xs-offset-4 col-sm-3 col-sm-offset-0">
										<div class="profile_image">
											<img class="img-responsive" src="<?php echo SITE_ROOT.'uploads/profile/'.$userImg?>">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="red_div profile_red">
					<h3>تحميل بلدي</h3>
				</div>
				<div class="loadMyUploads">
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
		loadMyUploads();
		 $("#locality").geocomplete({
          country: 'ae'
        }).on("geocode:result", function(event, result){ 
			valideUpload();
		 });
		//jQuery(".mylightbox").html5lightbox();
		$(".bootstrap-filestyle input").attr("placeholder", "Attach Your File");
        $(".profile_section").append('<div class="selectedfile"></div>');
		$(".close1").click(function(){
		$(".nav_section_slides").removeClass("nav_toggle");
	});
	$("#editUserModalForm").validate({
		rules: {
			u_name: "required",
			u_emirates: "required",
			u_password: {minlength: 6}, 
			u_con_password: { equalTo: "#u_password" }
		},
		messages: {
			u_name: 'Can\'t be empty',
			u_emirates: 'Can\'t be empty',
			u_password: {minlength:'Password must be at least 6 characters long'},
			u_con_password:'Please enter the same password as above'
		}
	});
	});
	
	$(function(){
     var $container = $('.grid');
     $container.imagesLoaded(function(){
     $container.masonry({
        itemSelector: '.grid-item'
      });
    });
	});
	$('.file_browse').on('change', function(){ 
		 var fileCount = this.files.length;
		 	slectedDiv	=	$(".selectedfile");
			
		if(fileCount	<= 1){
		 $('.upload-title p').text(fileCount+' file selected').css({"color" : "#31708f"});
		 }else{
			$('.upload-title p').text(fileCount+' files selected').css({"color" : "#31708f"});
		 }
	});
	$(".upoad-btn").on("click",function(e){
		  e.preventDefault();
		$(".upload_preloader").show();
		$(".uploadWidget").hide();
		$("#uploadPhotos").ajaxForm(
			{
				target: '#preview_uploads',
				success:successCall_upload
			}).submit();
	});
	function valideUpload(){
		var filesCounts	=	$('.file_browse').val();
		var insertvalue	=	$("#locality").val();
		if(filesCounts != '' && insertvalue != '' && insertvalue.indexOf("United Arab Emirates")>=0){
			$(".upoad-btn").removeAttr( "disabled" );
			$(".noifyError").hide();
			$('[data-toggle="popover"]').popover('hide');
			
		}else{
			$(".upoad-btn").attr( "disabled", "disabled" );
			$(".noifyError").show();
			$('[data-toggle="popover"]').popover('show');
		}
		
		$(".white_overlay").scroll(function(){
			$('[data-toggle="popover"]').popover('hide');
		});
	}
	function successCall_upload(){
		$(".upload_preloader").hide();
		$(".uploadWidget").show();	
		var outstr	=	$("#uploadStatus").val();
		if(outstr==='1'){
			$(".upload-title p").text('تم التحميل بنجاح').css({"color" : "#00742d"});
			$("#locality").val('');
			$("#photo_descr").val('');
			$("#photo_file").val('');
			loadMyUploads();
		}else{
			return false;
		}
		//return false;
	}
	function loadMyUploads(){
		$(".loadMyUploads").load('<?php echo SITE_ROOT?>ajax/my_upload_photos.php',function (){
			$(".mylightbox").html5lightbox();
			    var $container = $('.grid');
				 $container.imagesLoaded(function(){
				 $container.masonry({
					itemSelector: '.grid-item'
				  });
				});
		});
	}
function PreviewImage(no) {
	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById("uploadImage"+no).files[0]);
	
	oFReader.onload = function (oFREvent) {
		document.getElementById("uploadPreview"+no).src = oFREvent.target.result;
	};
}
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
<div class="modal fade editModal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">شارك في يوم العلم</h4>
      </div>
      <div class="modal-body">
	  <?php
	 if($_SESSION['edit_err'] ==1){
	  echo '<div class="alert alertClose alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alertClose" aria-label="Close"><span aria-hidden="true">&times;</span></button>تم التحديث بنجاح</div><script>$( ".alertClose" ).delay( 4000 ).slideUp( 400 );</script>';
	 }else if($_SESSION['edit_err'] ==2){
		 echo '<div class="alert alertClose alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alertClose" aria-label="Close"><span aria-hidden="true">&times;</span></button>Please fill the required fields</div><script>$( ".alertClose" ).delay( 4000 ).slideUp( 400 );</script>';
	 }
	  ?>
       <div class="profile-pic">
       		<form action="access/edit_user.php" method="post" enctype="multipart/form-data" id="editUserModalForm" >
            <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                        <label>الاسم الكامل</label>
                        <input type="text" class="form-control" value="<?php echo $objCommon->html2text($getUserDetials['name'])?>" name="u_name" id="u_name" placeholder="الاسم الكامل">
                    </div>
					<div class="form-group">
                        <label>كتابة كلمة المرور الجديدة</label>
                        <input type="password" class="form-control" placeholder="كتابة كلمة المرور الجديدة" name="u_password" id="u_password" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label>اعد كتابة كلمة المرور الجديدة</label>
                        <input type="password" class="form-control" placeholder="اعد كتابة كلمة المرور الجديدة" name="u_con_password" id="u_con_password" autocomplete="off">
                    </div>
              </div>
            <div class="col-sm-4">
                <div class="profie-pics"> 
                    <img id="uploadPreview1" src="<?php echo SITE_ROOT.'uploads/profile/'.$userImg?>"/><br />
                    <div class="fileUpload btn btn-primary">
                        <span>Upload</span>
                        <input id="uploadImage1" type="file" class="upload" name="u_image"  onchange="PreviewImage(1);" style="" />
                    </div>
              </div>
          </div>
       </div>
       <div class="row margin-it">
            <div class="col-sm-12">
            <div class="form-group">
                    <label>الإمارات</label>
					<select name="u_emirates"  id="u_emirates" class="form-control">
						<option value="">الإمارات</option>
						<?php
						foreach($getAllEmirates as $allEmirates){
						?>
						<option value="<?php echo $allEmirates['e_id']?>" <?php echo ($allEmirates['e_id']==$getUserDetials['emirates'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allEmirates['e_name_ar'])?></option>
						<?php
						}
						?>
					</select>
                </div>
            </div>
            <div class="col-sm-12">
				<input type="hidden" name="langSwitch" value="ar" />
            	<button type="submit" class="btn btn-primary loginReg">تسجيل</button>
            </div>
       </div>
       </form>
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>-->
    </div>
  </div>
</div>
<?php
if($_SESSION['edit_err'] != ''){
?>
	<script type="text/javascript">
		$(".editModal").modal('show');
	</script>
<?php
	unset($_SESSION['edit_err']);
}
?>
