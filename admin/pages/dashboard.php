<?php
include_once("../class/users.php");
$objUsers						=	new users();
$getUsersCount				   =	$objUsers->count();
$getUploadCountMore			  =	$objUsers->getRowSql("SELECT count(f_id) AS totalUploads FROM flags");
$getUploadCount				  =	$objUsers->getRowSql("SELECT count(f_id) AS countReadStatus FROM  flags WHERE f_admin_read_status=0");
$getEmiratesParti				=	$objUsers->listQuery("SELECT e.e_name_eng,e.e_id,e.e_alias,user.countUser FROM emirates AS e LEFT JOIN (SELECT count(user_id) AS countUser,emirates FROM users GROUP BY emirates) AS user ON e.e_id = user.emirates ORDER BY e.e_id");
$emiratesArr		  			 =	array('abu-dhabi'=>"abu dhabi",'dubai'=>"dubai",'sharjah'=>"sharjah",'ajman'=>"ajman",'umm-al-quwain'=>"umm al quwain",'ras-al-khaimah'=>"ras al-khaimah",'fujairah'=>"fujairah");
?>
<link rel="stylesheet" href="css/morris.css">
<script src="js/morris.js"></script>
<script src="js/raphael-min.js"></script>
<div class="row">
  <div class="col-lg-6">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Participants</h5>
      </div>
      <div class="ibox-content">
        <h1 class="no-margins"><?php echo number_format($getUsersCount)?></h1>
      	<div class="stat-percent font-bold text-navy"><a href="?page=participates.php" class="text-navy">View</a></div>
        <small>Total Participants</small>
	  </div>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="ibox float-e-margins">
      <div class="ibox-title"> <?php if($getUploadCount['countReadStatus']>0){ ?><span class="label no-bg pull-right text-danger">New</span><?php } ?>
        <h5>Uploads</h5>
      </div>
      <div class="ibox-content">
        <h1 class="no-margins"><?php echo $getUploadCountMore['totalUploads']?></h1>
		<div class="stat-percent font-bold text-navy"><a href="?page=flag_uploads.php" class="text-navy">View</a></div>
        <small>Total Uploaded Files</small> </div>
    </div>
  </div>
</div>
<div class="ibox-title clear">
	<h5><i class="fa fa-picture-o"></i> Emirates</h5>
</div>
<br />
<div class="row">
<?php
foreach($getEmiratesParti as $allEm){
	$aliasEmr					 =	$allEm['e_alias'];
	$countImagesList			  =	$objUsers->getRowSql("SELECT count(f_id) AS uploadCount FROM flags  WHERE f_location LIKE '%".$emiratesArr[$aliasEmr]."%' ");
	$participateCount			 =	($allEm['countUser']>0)?$allEm['countUser']:'0';
	$uploadImgCount			   =	($countImagesList['uploadCount']>0)?$countImagesList['uploadCount']:'0';			
	$dataPiChart				  .=	"{x: '".$objCommon->html2text($allEm['e_name_eng'])."', y: ".$participateCount.", z: ".$uploadImgCount.", },";
?>
	<div class="col-lg-3">
		<div class="ibox float-e-margins">
		  <div class="ibox-title">
			<h5><?php echo $objCommon->html2text($allEm['e_name_eng'])?></h5>
		  </div>
		  <div class="ibox-content">
		  	<div class="row">
				<div class="col-md-6">
					<h1 class="no-margins"><?php echo $participateCount?></h1>
					<small>Participants</small> 
				</div>
				<div class="col-md-6">
					<h1 class="no-margins"><?php echo $uploadImgCount?></h1>
					<small>uploads</small> 
				</div>
			</div>
			
		  </div>
		</div>
	  </div>
<?php
}
$dataPiChart=trim($dataPiChart,",");
?>
</div>
<div class="row">
<div class="col-sm-12">
		<section class="panel">
			<header class="panel-heading">
				<!--Bar Chart-->
			<span class="tools pull-right">
				<!--<a href="javascript:;" class="fa fa-chevron-down"></a>
				<a href="javascript:;" class="fa fa-times"></a>-->
			 </span>
			</header>
			<div class="panel-body">
				<div id="graph-bar"></div>
			</div>
		</section>
	</div>
</div>
<script>
$(document).ready(function(){
	Morris.Bar({
    element: 'graph-bar',
    data: <?php echo "[".$dataPiChart."]"?>,
    xkey: 'x',
    ykeys: ['y', 'z'],
    labels: ['No of Participants', 'No.of Uploads'],
    barColors:['#6dc5a3','#788ba0']
	});
});
</script>
