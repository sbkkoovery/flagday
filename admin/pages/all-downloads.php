<?php
include_once("../class/".$LANGUAGE."downloads.php");
$objSliders	=	new downloads();
?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-picture-o"></i> Downloads</h5>
        <div class="pull-right"><a href="./?page=add-downloads.php" class="btn btn-default customPadding">Add New Downloads</a></div>
      </div>
      <div class="ibox-content">
      	<?php echo $objCommon->displayMsg(); ?>
      	<div class="table-responsive">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
        	<thead>
                <tr>
                    <th>No</th>
                    <th>File Name</th>
					<th>Alias</th>
                    <th>File</th>
                    <th>Action</th>
                </tr>
            </thead>
        	<tfoot>
                <tr>
                    <th>No</th>
                    <th>File Name</th>
					<th>Alias</th>
                    <th>File</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody>
            	<?php 
					$sliderList	=	$objSliders -> getAll();
					if(count($sliderList)>0){
					$i=1;
					foreach($sliderList as $slider){
				?>
            	<tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $slider['d_name']; ?></td>
					<td><?php echo $slider['d_alias']; ?></td>
                    <td><a href="../uploads/downloads/<?php echo $slider['d_image']; ?>" download  target="_blank" >Download</a></td>
                    <td><a href="?page=add-downloads.php&sliderId=<?php echo $slider['d_id']; ?>" class="actionLink fa fa-pencil-square" title="Edit"></a><a href="access/delete-downloads.php?sliderId=<?php echo $slider['d_id']; ?>" onclick="return confirm('You want to delete this content..?');" class="actionLink fa fa-trash" title="Delete"></a></td>                	
                </tr>
                <?php $i++;}}else{?>
                <tr>
                    <td colspan="5">No Results Found..</td>
                </tr>    
                <?php }?>
            </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>
