<?php 
include_once("../class/".$LANGUAGE."slider.php");
$objSlider	=	new slider();
if(isset($_GET['sliderId'])){
	$sliderId		=	$objCommon->esc($_GET['sliderId']);
	$sliderDetails	=	$objSlider->getRow("slider_id=$sliderId");
}
?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-plus-circle"></i> Add Slider</h5>
      </div>
      <div class="ibox-content">
      	<?php echo $objCommon->displayMsg(); ?>
      	<div class="form-content">
            <form class="form-horizontal" method="post" action="access/add-slider.php" enctype="multipart/form-data">
				<?php if(isset($_GET['sliderId'])){?>
                <input type="hidden" name="sliderId" value="<?php echo $sliderId ?>" />
                <?php }?>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Slider Name</label>
                    <div class="col-sm-10">
                      <input type="text" name="slider_name" class="form-control" id="inputPassword3" value="<?php echo $sliderDetails['slider_name']; ?>" placeholder="Slider Name">
                    </div>
                  </div>
                  <?php if($sliderDetails['slider_image']!=""){?>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-sm-10">
                      <img src="../uploads/home_slider/<?php echo $sliderDetails['slider_image']; ?>" style="margin-left:0px; max-width:200px;" />
                    </div>
                  </div>
                  <?php }?>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Slider Image <?php /*?><span style="font-weight:normal; font-size:9px;">(Best Size 255x175 px)</span><?php */?></label>
                    <div class="col-sm-10">
                      <input name="sliderImage" type="file" class="fileField" />
                    </div>
                  </div>
              	<div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" name="submit" class="btn btn-default"><?php echo (isset($_GET['sliderId']))?'Update':'Add'; ?> Slider</button>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
