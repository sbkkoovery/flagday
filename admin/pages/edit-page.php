<?php 
include_once("../class/".$LANGUAGE."pages.php");
$objPages	=	new pages();
if(isset($_GET['pageId'])){
	$pageId				=	$objCommon->esc($_GET['pageId']);
	$pageDetails		=	$objPages->getRow("page_id=$pageId");
}
?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-plus-circle"></i> Edit Pages</h5>
        <div class="pull-right"><a href="./?page=all-pages.php" class="btn btn-default customPadding">All Pages</a></div>
      </div>
      <div class="ibox-content">
        <?php echo $objCommon->displayMsg(); ?>
      	<div class="form-content">
            <form class="form-horizontal" method="post" action="access/edit-page.php">
              <?php if(isset($_GET['pageId'])){?>
              <input type="hidden" name="pageId" value="<?php echo $_GET['pageId']; ?>" />
              <?php }?>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Page Title</label>
                <div class="col-sm-10">
                  <input name="page_title" type="text" class="form-control" value="<?php echo $pageDetails['page_title']; ?>" />
                </div>
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Page Content</label>
                <div class="col-sm-10">
                  <textarea cols="40" rows="5" class="ckeditor" name="page_content"><?php echo $objCommon->displayEditor($pageDetails['page_content']); ?></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-default">Update Page</button>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
