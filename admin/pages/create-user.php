<?php 
include_once("../class/emirates.php");
include_once("../class/page_url.php");
$objEmirates			 =	new emirates();
$objPageUrl			  =	new page_url();
$getEmirates			 =	$objEmirates->getAll();
$getPageUrl			  =	$objPageUrl->getAll("","pu_name");
$editId				  =	'';
if(isset($_GET['editId'])){
	$editId			  =	$objCommon->esc($_GET['editId']);
	$getDetails	   	  =	$objAdmin->getRow("admin_type=2 and aminId=$editId");
	$editFlagAccess	  =	$getDetails['flag_access'];
	if($editFlagAccess){
		$explFlagAccess  =	explode(",",$editFlagAccess);
		$explFlagAccess  =	array_filter($explFlagAccess);
	}
	$editPageAccess	  =	$getDetails['page_access'];
	if($editPageAccess){
		$explPageAccess  =	explode(",",$editPageAccess);
		$explPageAccess  =	array_filter($explPageAccess);
	}
}
?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-plus-circle"></i> Add Users</h5>
      </div>
      <div class="ibox-content">
      	<?php
		echo $objCommon->displayMsg();
		?>
      	<div class="form-content">
            <form class="form-horizontal" method="post" action="access/add-users.php" enctype="multipart/form-data">
				<div class="form-group">
					<label class="col-sm-2 control-label">Name</label>
					<div class="col-sm-10">
						<input type="text" name="admin_name" class="form-control"  value="<?php echo $getDetails['admin_name']; ?>" placeholder="Name">
					</div>
				 </div>
				 <div class="form-group">
					<label class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
						<input type="email" name="email" id="email" class="form-control"  value="<?php echo $getDetails['email']; ?>" placeholder="Email">
					</div>
				 </div>
				 <?php if($getDetails['aminId'] ==''){?>
                
                  <div class="form-group">
					<label class="col-sm-2 control-label">Username</label>
					<div class="col-sm-10">
						<input type="text" name="username" id="username" class="form-control"   placeholder="Username">
					</div>
				 </div>
				 <div class="form-group">
					<label class="col-sm-2 control-label">Password</label>
					<div class="col-sm-10">
						<input type="password" name="password" class="form-control" >
					</div>
				 </div>
				 <?php }else{
					 ?>
					 <input type="hidden" name="editId" value="<?php echo $editId ?>" />
					 <?php
				 }
				 ?>
				  <div class="form-group">
					<label class="col-sm-2 control-label">Emirate Access</label>
					<div class="col-sm-10">
						<?php
						foreach($getEmirates as $allEmirates){
							if(count($explFlagAccess) >0){
								$selEmirate			=	(in_array($allEmirates['e_id'],$explFlagAccess))?'checked="checked"':'';
							}else{
								$selEmirate			=	'';
							}
						?>
						<label><input type="checkbox" name="emirate[]" value="<?php echo $allEmirates['e_id']?>"  <?php echo $selEmirate?> /> &nbsp;<?php echo $allEmirates['e_name_eng']?></label>&nbsp;&nbsp;&nbsp;&nbsp;
						<?php
						}
						?>
					</div>
				 </div>
				 <div class="form-group">
					<label class="col-sm-2 control-label">page Access</label>
					<div class="col-sm-10">
						<?php
						foreach($getPageUrl as $allPages){
							if(count($explPageAccess) >0){
								$selpages			=	(in_array($allPages['pu_id'],$explPageAccess))?'checked="checked"':'';
							}else{
								$selpages			=	'';
							}
						?>
						<label><input type="checkbox" name="pages[]" value="<?php echo $allPages['pu_id']?>"  <?php echo $selpages?> /> &nbsp;<?php echo $allPages['pu_name']?></label>&nbsp;&nbsp;&nbsp;&nbsp;
						<?php
						}
						?>
					</div>
				 </div>
              	<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
					  <button type="submit" name="submit" class="btn btn-default"><?php echo (isset($_GET['editId']))?'Update':'Add'; ?> User</button>
					</div>
              	</div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
