<?php 
include_once("../class/".$LANGUAGE."slider.php");
$objSliders	=	new slider();
?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-picture-o"></i> Sliders</h5>
        <div class="pull-right"><a href="./?page=add-slider.php" class="btn btn-default customPadding">Add New Slider</a></div>
      </div>
      <div class="ibox-content">
      	<?php echo $objCommon->displayMsg(); ?>
      	<div class="table-responsive">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
        	<thead>
                <tr>
                    <th>No</th>
                    <th>Slider Name</th>
                    <th>Slider Image</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
        	<tfoot>
                <tr>
                    <th>No</th>
                    <th>Slider Name</th>
                    <th>Slider Image</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody>
            	<?php 
					$sliderList	=	$objSliders -> getAll();
					if(count($sliderList)>0){
					$i=1;
					foreach($sliderList as $slider){
				?>
            	<tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $slider['slider_name']; ?></td>
                    <td><img src="../uploads/home_slider/<?php echo $slider['slider_image']; ?>" style="width:100px; margin-left:0px;" /></td>
                    <td><?php echo ($slider['status']==1)?'Enabled':'Disabled'; ?></td>
                    <td><a href="?page=add-slider.php&sliderId=<?php echo $slider['slider_id']; ?>" class="actionLink fa fa-pencil-square" title="Edit"></a><a href="access/delete-slider.php?sliderId=<?php echo $slider['slider_id']; ?>" onclick="return confirm('You want to delete this slider..?');" class="actionLink fa fa-trash" title="Delete"></a></td>                	
                </tr>
                <?php $i++;}}else{?>
                <tr>
                    <td colspan="5">No Results Found..</td>
                </tr>    
                <?php }?>
            </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>
