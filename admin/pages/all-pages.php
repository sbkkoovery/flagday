<?php 
include_once("../class/".$LANGUAGE."pages.php");
$objPages	=	new pages();
$allPages	=	$objPages->getAll();
?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title clear">
        <h5><i class="fa fa-files-o"></i> Pages</h5>
      </div>
      <div class="ibox-content">
      	<?php echo $objCommon->displayMsg(); ?>
      	<div class="table-responsive">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
        	<thead>
                <tr>
                    <th width="5">No</th>
                    <th>Page Name</th>
                    <th width="150">Status</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
        	<tfoot>
                <tr>
                    <th>No</th>
                    <th>Page Name</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody>
            	<?php 
				if(count($allPages)>0){
				$i=1;
				foreach($allPages as $page){?>
            	<tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $page['page_title']; ?></td>
                    <td><?php echo ($page['status']==1)?'Enabled':'Disabled'; ?></td>
                    <td><a href="?page=edit-page.php&pageId=<?php echo $page['page_id']; ?>" class="actionLink fa fa-pencil-square" title="Edit"></a></td>                	
                </tr>
                <?php $i++;}
				}else{?>
                <tr>
                	<td colspan="6">There is no results found.. </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>
