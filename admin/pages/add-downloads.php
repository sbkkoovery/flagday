<?php 
include_once("../class/".$LANGUAGE."downloads.php");
$objDownloads	=	new downloads();
if(isset($_GET['sliderId'])){
	$sliderId		=	$objCommon->esc($_GET['sliderId']);
	$sliderDetails	=	$objDownloads->getRow("d_id=$sliderId");
}
?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-plus-circle"></i> Add Downloads</h5>
      </div>
      <div class="ibox-content">
      	<?php echo $objCommon->displayMsg(); ?>
      	<div class="form-content">
            <form class="form-horizontal" method="post" action="access/add-downloads.php" enctype="multipart/form-data">
				<?php if(isset($_GET['sliderId'])){?>
                <input type="hidden" name="sliderId" value="<?php echo $sliderId ?>" />
                <?php }?>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">File Name</label>
                    <div class="col-sm-10">
                      <input type="text" name="d_name" class="form-control" id="inputPassword3" value="<?php echo $sliderDetails['d_name']; ?>" placeholder="Download Name">
                    </div>
                  </div>
				  <div class="form-group">
                    <label class="col-sm-2 control-label">Alias</label>
                    <div class="col-sm-10">
                      <input type="text" name="d_alias" class="form-control"  value="<?php echo $sliderDetails['d_alias'];?>" placeholder="Alias">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">File</label>
                    <div class="col-sm-10">
                      <input name="sliderImage" type="file" class="fileField" />
                    </div>
                  </div>
              	<div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" name="submit" class="btn btn-default"><?php echo (isset($_GET['sliderId']))?'Update':'Add'; ?> Downloads</button>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
