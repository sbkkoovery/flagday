<?php 
include_once("../class/users.php");
$objUsers	=	new users();
$sql        = "SELECT user.*,emi.e_name_eng FROM users AS user LEFT  JOIN emirates AS emi ON user.emirates = emi.e_id";
$num_results_per_page 	 = 20;
$num_page_links_per_page  = 5;
$pg_param = "";
$pagesection='';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$userList		=	$objUsers->listQuery($paginationQuery);
?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title clear">
        <h5><i class="fa fa-book"></i> Participants</h5>
      </div>
      <div class="ibox-content">
      	<?php 
		echo $objCommon->displayMsg();
		?>
      	<div class="table-responsive">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
        	<thead>
                <tr>
                    <th width="5">No</th>
                    <th>Name</th>
                    <th>Email</th>
					<th>Emirate</th>
                    <th>Status</th>
					<th>Uploads</th>
                </tr>
            </thead>
        	<tfoot>
                <tr>
                    <th width="5">No</th>
                    <th>Name</th>
                    <th>Email</th>
					<th>Emirate</th>
                    <th>Status</th>
					<th>Uploads</th>
                </tr>
            </tfoot>
            <tbody>
            	<?php 
				if(count($userList)>0){
				$i=1;
				foreach($userList as $all){?>
            	<tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $objCommon->html2text($all['name']); ?></td>
                    <td><?php echo $objCommon->html2text($all['email']); ?></td>
					<td><?php echo $objCommon->html2text($all['e_name_eng']); ?></td>
                    <td>
					<input type="checkbox" class="js-switch" <?php echo ($all['status']==1)?'checked':''; ?> onchange="changeStatus('<?php echo $all['user_id']; ?>','<?php echo ($all['status']==1)?0:1;?>')" />
					<?php /*?><a href="access/change-user-status.php?userid=<?php echo $all['user_id']; ?>&status=<?php echo ($all['status']==1)?0:1; ?>" title="Status" class="<?php echo ($all['status']==1)?'enable':'disable'; ?>"><i class="fa fa-circle"></i></a><?php */?>
					</td>
					<td><a href="?page=flag_uploads.php&userId=<?php echo $all['user_id']; ?>" class="text-navy">View</a></td>          	
                </tr>
                <?php $i++;}
				}else{?>
                <tr>
                	<td colspan="8">There is no results found.. </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
elems.forEach(function(html) {
  var switchery = new Switchery(html, { color: 'rgb(100, 189, 99)', secondaryColor: '#ff3333', jackColor: '#ffffff', jackSecondaryColor: '#ffffff',  size: 'small' });
});
//var switchery = new Switchery(elem, { color: 'rgb(100, 189, 99)', secondaryColor: '#ff3333', jackColor: '#ffffff', jackSecondaryColor: '#ffffff',  size: 'small' });
function changeStatus(userid,status){
	var that			=	this;
	if(userid != '' && status != ''){
		$.get('access/change-user-status.php',{userid:userid,status:status},function(data){
			//alert(data);
			//that.prop('checked');
		});
	}
}
</script>
