<?php 
include_once("../class/flags.php");
$objFlags				 =	new flags();
$fIid				     =	$_GET['f_id'];
$sql        	 		  =	"SELECT * FROM flag_more  WHERE f_id=".$fIid."  ORDER BY fm_id DESC";
$num_results_per_page 	 =	20;
$num_page_links_per_page  =	5;
$pg_param 				 =	"";
$pagesection			  =	'';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$userList				 =	$objFlags->listQuery($paginationQuery);
$getFlagDetails		   =	$objFlags->getRow("f_id=".$fIid);
?>
<script type="text/javascript" src="../js/html5lightbox.js"></script>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title clear">
        <h5><i class="fa fa-book"></i> Flag Uploads Files - Location: <?php echo $objCommon->html2text($getFlagDetails['f_location'])?></h5>
		<div class="pull-right"><a href="./?page=flag_uploads.php" class="btn btn-default customPadding">Back</a></div>
      </div>
      <div class="ibox-content">
      	<?php echo $objCommon->displayMsg(); ?>
      	<div class="table-responsive">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
        	<thead>
                <tr>
                    <th width="5">No</th>
                    <th>Type</th>
                    <th>File</th>
                    <th>Status</th>
                </tr>
            </thead>
        	<tfoot>
                <tr>
                    <th width="5">No</th>
                    <th>Type</th>
                    <th>File</th>
                    <th>Status</th>
                </tr>
            </tfoot>
            <tbody>
            	<?php 
				if(count($userList)>0){
				$i=1;
				foreach($userList as $all){
				$uploadType		 =	$all['fm_type'];
				if($uploadType ==1){
					$flag_images	=	$all['fm_url'];
				}else if($uploadType ==2){
					$flag_url	   =	$all['fm_url'];
					$flag_images	=	$objCommon->getThumb($all['fm_thumb']);
				}	
					?>
            	<tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo ($all['fm_type']==1)?'Image':'Video'; ?></td>
                    <td>
					<?php
					if($uploadType ==1){
						?>
					<a href="<?php echo '../uploads/flags_images/'.$flag_images?>" class="mylightbox"   title="<?php echo $objCommon->html2text($getFlagDetails['f_title'])?>"><img class="img-responsive" src="<?php echo '../uploads/flags_images/'.$flag_images?>" width="100"></a>
					<?php
					}else if($uploadType ==2){
					?>
					<a href="JavaScript:html5Lightbox.showLightbox(2, '../uploads/flags_images/<?php echo $flag_url?>', '<?php echo $objCommon->html2text($getFlagDetails['f_title'])?>', 800, 450, '../uploads/flags_images/<?php echo $flag_url?>');"><img class="img-responsive" src="<?php echo '../uploads/flags_images/'.$flag_images?>" width="100"><span class="videoIcon"><i class="fa fa-video-camera"></i></span></a>
					<?php
					}
					?>
					</td>
                    <td>
					<input type="checkbox" class="js-switch" <?php echo ($all['fm_status']==1)?'checked':''; ?> onchange="changeStatus('<?php echo $all['fm_id']; ?>','<?php echo ($all['fm_status']==1)?0:1;?>')" />
					<?php /*?><a href="access/change-flag-more-status.php?fm_id=<?php echo $all['fm_id']; ?>&status=<?php echo ($all['fm_status']==1)?0:1; ?>" title="Status" class="<?php echo ($all['fm_status']==1)?'enable':'disable'; ?>"><i class="fa fa-circle"></i></a><?php */?>
					</td>       	
                </tr>
                <?php $i++;}
				}else{?>
                <tr>
                	<td colspan="8">There is no results found.. </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
		$(".mylightbox").html5lightbox();
	});
	</script>
<script>
var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
elems.forEach(function(html) {
  var switchery = new Switchery(html, { color: 'rgb(100, 189, 99)', secondaryColor: '#ff3333', jackColor: '#ffffff', jackSecondaryColor: '#ffffff',  size: 'small' });
});
//var switchery = new Switchery(elem, { color: 'rgb(100, 189, 99)', secondaryColor: '#ff3333', jackColor: '#ffffff', jackSecondaryColor: '#ffffff',  size: 'small' });
function changeStatus(fm_id,status){
	var that			=	this;
	if(fm_id != '' && status != ''){
		$.get('access/change-flag-more-status.php',{fm_id:fm_id,status:status},function(data){
			//alert(data);
			//that.prop('checked');
		});
	}
}
</script>
