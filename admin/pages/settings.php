<?php 
include_once("../class/settings.php");
$objSettings	=	new settings();
$webSettings	=	$objSettings->getRow("setting_id=1");
?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title clear">
        <h5><i class="fa fa-gear"></i> Settings</h5>
        <div class="pull-right"><a href="./?page=add_city.php" class="btn btn-default customPadding">Add New City</a></div>
      </div>
      <div class="ibox-content">
      	<?php echo $objCommon->displayMsg(); ?>
      	<div class="table-responsive">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
        	<thead>
                <tr>
                    <th width="5">No</th>
                    <th>Settings Name</th>
                    <th width="150">Status</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
        	<tfoot>
                <tr>
                    <th width="5">No</th>
                    <th>Settings Name</th>
                    <th width="150">Status</th>
                    <th width="100">Action</th>
                </tr>
            </tfoot>
            <tbody>
            	<tr>
                    <td>1</td>
                    <td>Website Status</td>
                    <td><?php echo ($webSettings['status']==1)?'Enabled':'Disabled'; ?></td>
                    <td><a href="access/change-web-status.php?Id=<?php echo $webSettings['setting_id']; ?>&status=<?php echo ($webSettings['status']==1)?0:1; ?>" class="btn btn-default">Change</a></td>                	
                </tr>
            </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>
