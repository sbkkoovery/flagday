<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-plus-circle"></i>Change Password</h5>
        
      </div>
      <div class="ibox-content">
        <?php echo $objCommon->displayMsg(); ?>
      	<div class="form-content">
            <form class="form-horizontal" method="post" action="access/change-password.php">
              <div class="form-group">
                <label class="col-sm-2 control-label">New Password</label>
                <div class="col-sm-10">
                    <input name="password" type="password" class="form-control" />
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label">Confirm Password</label>
                <div class="col-sm-10">
                    <input name="con_password" type="password" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-default">Submit</button>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript">
$(document).ready(function(){
	$(".add_schedule").click(function(){
		var tabText	=	'<tr><td><input type="text" name="sch_start[]" class="form-control datepicker" placeholder="Start Date" /></td><td><input type="text" name="sch_end[]" class="form-control datepicker" placeholder="End Date"/></td><td><input type="text" name="sch_city[]" class="form-control " placeholder="City Name"/></td><td><input type="text" name="sch_cost[]" class="form-control " placeholder="Cost"/></td><td><a href="javascript:;" onclick="closeSch(this);"><i class="fa fa-close"></i></a></td><td><a href="javascript:;" onclick="copySch(this);">copy</a></td></tr>';
		$(".add_tab_sch").append(tabText);
		$( ".datepicker" ).datepicker({dateFormat:"dd-mm-yy"});
	});
});
function closeSch(s){
	$(s).parent().parent().remove();
}
function copySch(a){
	var parentCopy		=	$(a).parent().parent().clone();
	$(a).parent().parent("tr").after(parentCopy);
}
</script>