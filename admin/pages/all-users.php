<?php
$allPages	=	$objAdmin->getAll("admin_type=2");
?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title clear">
        <h5><i class="fa fa-files-o"></i> Users</h5>
      </div>
      <div class="ibox-content">
      	<?php echo $objCommon->displayMsg(); ?>
      	<div class="table-responsive">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
        	<thead>
                <tr>
                    <th width="5">No</th>
                    <th>Name</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
        	<tfoot>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody>
            	<?php 
				if(count($allPages)>0){
				$i=1;
				foreach($allPages as $page){?>
            	<tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $page['admin_name']; ?></td>
                    <td><a href="?page=create-user.php&editId=<?php echo $page['aminId']; ?>" class="actionLink fa fa-pencil-square" title="Edit"></a><a href="access/delete-user.php?delId=<?php echo $page['aminId']; ?>" onclick="return confirm('You want to delete this slider..?');" class="actionLink fa fa-trash" title="Delete"></a></td>                	
                </tr>
                <?php $i++;}
				}else{?>
                <tr>
                	<td colspan="6">There is no results found.. </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>
