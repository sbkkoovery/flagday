<?php 
include_once("../class/settings.php");
$objSettings	=	new settings();
$settingAll		=	$objSettings->getAll();
$settings		=	$settingAll[0];
?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-plus-circle"></i> Add Settings</h5>
      </div>
      <div class="ibox-content">
      	<?php echo $objCommon->displayMsg(); ?>
      	<div class="form-content">
            <form class="form-horizontal" method="post" action="access/settings.php">
              <?php if(isset($_GET['settingId'])){?>
              <input type="hidden" name="settingId" value="<?php echo $settingId; ?>" />
              <?php }?>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label"><?php echo $settings['setting_name']; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="setting_link" class="form-control" id="inputPassword3" value="<?php echo $settings['setting_link']; ?>" placeholder="<?php echo $settings['setting_name']; ?>">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-default">Add Settings</button>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
