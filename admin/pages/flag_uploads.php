<?php 
include_once("../class/flags.php");
$objFlags				 =	new flags();
$userId				   =	$_GET['userId'];
if($userId){
	$sqlCond		  	  =	" AND flag.user_id =".$userId;
}
//-------------------------------------------------------------------------------------------------------------------
if($adminDetails['admin_type']==2){
	$flagAccessStr			=	$adminDetails['flag_access'];
	if($flagAccessStr != ''){
		$flagAccessExpl	   =	explode(",",$flagAccessStr);
		$flagAccessExpl	   =	array_filter($flagAccessExpl);
	}
	$emiratesArr		  	  =	array('1'=>"abu dhabi",'3'=>"dubai",'6'=>"sharjah",'2'=>"ajman",'7'=>"umm al quwain",'5'=>"ras al-khaimah",'4'=>"fujairah");
	if(count($flagAccessExpl)== '7' || count($flagAccessExpl) == '0'){
		$sql        	 	  =	"SELECT flag.*,user.name FROM flags AS flag LEFT JOIN users AS user ON flag.user_id = user.user_id WHERE f_id !='' ".$sqlCond."  ORDER BY flag.f_created DESC";
	}else{
		$sql_Access		   .=	" AND (";
		$keyFlag			   =	0;
		foreach($flagAccessExpl as $allflagAccess){
			if($keyFlag==0){
				$sql_Access   .=	" flag.f_location LIKE '%".$emiratesArr[$allflagAccess]."%' ";
			}else{
				$sql_Access   .=	" OR flag.f_location LIKE '%".$emiratesArr[$allflagAccess]."%' ";
			}
			$keyFlag++;
		}
		$sql_Access		   .=	" ) ";
		$sql        	 	   =	"SELECT flag.*,user.name FROM flags AS flag LEFT JOIN users AS user ON flag.user_id = user.user_id WHERE f_id !='' ".$sqlCond.$sql_Access."  ORDER BY flag.f_created DESC";
	}
}else{
	$sql        	 		  =	"SELECT flag.*,user.name FROM flags AS flag LEFT JOIN users AS user ON flag.user_id = user.user_id WHERE f_id !='' ".$sqlCond."  ORDER BY flag.f_created DESC";
}
//----------------------------------------------------------------------------------------------------------------------
$num_results_per_page 	 =	20;
$num_page_links_per_page  =	5;
$pg_param 				 =	"";
$pagesection			  =	'';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$userList				 =	$objFlags->listQuery($paginationQuery);
$objFlags->updateField(array("f_admin_read_status"=>1),"f_id !=''");
?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title clear">
        <h5><i class="fa fa-book"></i> Flag Uploads</h5>
      </div>
      <div class="ibox-content">
      	<?php echo $objCommon->displayMsg(); ?>
      	<div class="table-responsive">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
        	<thead>
                <tr>
                    <th width="5">No</th>
                    <th>Title</th>
                    <th>Location</th>
					<th>Uploaded by</th>
                    <th>Status</th>
					<th>Uploads</th>
                </tr>
            </thead>
        	<tfoot>
                <tr>
                     <th width="5">No</th>
                    <th>Title</th>
                    <th>Location</th>
					<th>Uploaded by</th>
                    <th>Status</th>
					<th>Uploads</th>
                </tr>
            </tfoot>
            <tbody>
            	<?php 
				if(count($userList)>0){
				$i=1;
				foreach($userList as $all){?>
            	<tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $objCommon->html2text($all['f_title']); ?></td>
                    <td><?php echo $objCommon->html2text($all['f_location']); ?></td>
					<td><?php echo $objCommon->html2text($all['name']); ?></td>
                    <td>
					<input type="checkbox" class="js-switch" <?php echo ($all['f_status']==1)?'checked':''; ?> onchange="changeStatus('<?php echo $all['f_id']; ?>','<?php echo ($all['f_status']==1)?0:1;?>')" />
					<?php /*?><a href="access/change-flag-status.php?f_id=<?php echo $all['f_id']; ?>&status=<?php echo ($all['f_status']==1)?0:1; ?>" title="Status" class="<?php echo ($all['f_status']==1)?'enable':'disable'; ?>"><i class="fa fa-circle"></i></a><?php */?>
					</td>
					<td><a href="?page=flag_uploads_more.php&f_id=<?php echo $all['f_id']?>" class="text-navy">View</a></td>          	
                </tr>
                <?php $i++;}
				}else{?>
                <tr>
                	<td colspan="8">There is no results found.. </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
elems.forEach(function(html) {
  var switchery = new Switchery(html, { color: 'rgb(100, 189, 99)', secondaryColor: '#ff3333', jackColor: '#ffffff', jackSecondaryColor: '#ffffff',  size: 'small' });
});
//var switchery = new Switchery(elem, { color: 'rgb(100, 189, 99)', secondaryColor: '#ff3333', jackColor: '#ffffff', jackSecondaryColor: '#ffffff',  size: 'small' });
function changeStatus(f_id,f_status){
	var that			=	this;
	if(f_id != '' && f_status != ''){
		$.get('access/change-flag-status.php',{f_id:f_id,f_status:f_status},function(data){
			//alert(data);
			//that.prop('checked');
		});
	}
}
</script>
