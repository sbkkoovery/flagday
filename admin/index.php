<?php 
@session_start();
ob_start();
$LANGUAGE	=	'';
if(isset($_SESSION['language'])){
	$LANGUAGE=$_SESSION['language'];
}
include_once("../class/common_class.php");
include_once("../class/pagination-class.php");
include_once("../class/admin.php");
$objAdmin	=	new admin();
$objCommon	=	new common();
if(!$objCommon->adminLogin()){
	header("location:login.php");
	exit();
}
$adminDetails	=	$objAdmin->getRow("aminId=".$_SESSION['admin']['ID']);
$page	    =	'dashboard';
if(isset($_GET['page'])&&$_GET['page']!=""){
	$page	=	$_GET['page'];
}
$pageName	=	"pages/".$page;
if(!file_exists($pageName)){
	$pageName	=	"pages/dashboard.php";
}
$pageurlId	   =	$objAdmin->getRowSql("select pu_id from page_url where pu_name='".$page."'");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Flag Day | Admin</title>
<link rel="shortcut icon" href="images/fevicon.png">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">
<!-- Morris -->
<link href="css/morris-0.css" rel="stylesheet">
<!-- Gritter -->
<link href="css/jquery.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<!-- Mainly scripts -->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery_011.js"></script>
<link rel="stylesheet" href="css/jquery-ui.css">
<script src="js/datepicker.js"></script>
<link rel="stylesheet" href="css/switchery.css" />
<script src="js/switchery.js"></script>
<script language="JavaScript" type="text/javascript">
$(function() {
$( ".datepicker" ).datepicker({dateFormat:"dd-mm-yy"});
});
$(function() {
	$( "#startDate" ).datepicker({defaultDate: "+1w",
		onClose: function( selectedDate ) {
			$( "#endDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#endDate" ).datepicker({defaultDate: "+1w",
		onClose: function( selectedDate ) {
			$( "#startDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<body class="pace-done">
<div class="pace  pace-inactive">
<div data-progress="99" data-progress-text="100%" style="width: 100%;" class="pace-progress">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element text-center"> <span style="color:#fff;">UAE Flag Day</span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                             </span> <span class="text-muted text-xs block"><?php echo $_SESSION['admin']['adminName']?> <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="?page=change-password.php">Password</a></li>
                                 <li class="divider"></li>
                                <li><a href="access/logout.php">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element"><img src="images/logo-smal.png" alt="Flagday"/></div>
                    </li>
                   <?php /*?> <li<?php echo ($page=="dashboard.php")?' class="active"':''; ?>>
                        <a href="?page=dashboard.php"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
                    </li><?php */?>
                    <li><a href="../"><i class="fa fa-star"></i> <span class="nav-label">Landing Page</span></a></li>
					 <li><a href="?page=dashboard.php"><i class="fa fa-home"></i> <span class="nav-label">Dashboard</span></a></li>
					 <li<?php echo ($page=="participates.php")?' class="active"':''; ?>>
                        <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Participants</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse<?php echo ($page=="participates.php")?' in':''; ?>">
                        	<li <?php echo ($page=="participates.php")?' class="active"':''; ?>><a href="?page=participates.php"><span class="fa fa-angle-right"></span> All Users</a></li>
                        </ul>
                    </li>
					 <li<?php echo ($page=="flag_uploads.php" || $page=="flag_uploads_more.php")?' class="active"':''; ?>>
                        <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Flag Uploads</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse<?php echo ($page=="flag_uploads.php" || $page=="flag_uploads_more.php")?' in':''; ?>">
                        	<li <?php echo ($page=="flag_uploads.php")?' class="active"':''; ?>><a href="?page=flag_uploads.php"><span class="fa fa-angle-right"></span> All Uploads</a></li>
                        </ul>
                    </li>
                    <li<?php echo ($page=="all-pages.php"||$page=="edit-page.php")?' class="active"':''; ?>>
                        <a href="#"><i class="fa fa-files-o"></i> <span class="nav-label">Pages</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse<?php echo ($page=="all-pages.php"||$page=="edit-page.php")?' in':''; ?>">
                        	<li <?php echo ($page=="all-pages.php"||$page=="edit-page.php")?' class="active"':''; ?>><a href="?page=all-pages.php"><span class="fa fa-angle-right"></span> All Pages</a></li>
                        </ul>
                    </li>
                    <li<?php echo ($page=="all-sliders.php"||$page=="add-slider.php")?' class="active"':''; ?>>
                        <a href="#"><i class="fa fa-picture-o"></i> <span class="nav-label">Sliders</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse<?php echo ($page=="all-sliders.php"||$page=="add-slider.php")?' in':''; ?>">
                        	<li <?php echo ($page=="all-sliders.php")?' class="active"':''; ?>><a href="?page=all-sliders.php">All Sliders</a></li>
                        	<li <?php echo ($page=="add-slider.php")?' class="active"':''; ?>><a href="?page=add-slider.php">Add Slider</a></li>
                        	<?php /*?><li><a href="?page=import_emirates.php">Import Emirates</a></li><?php */?>
                        </ul>
                    </li>
                    <li<?php echo ($page=="add-downloads.php" || $page=="all-downloads.php" )?' class="active"':''; ?>>
                        <a href="#"><i class="fa fa-download"></i> <span class="nav-label">Downloads</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse<?php echo ($page=="add-downloads.php")?' in':''; ?>">
							<li <?php echo ($page=="all-downloads.php")?' class="active"':''; ?>><a href="?page=all-downloads.php">All Downloads</a></li>
                        	<li <?php echo ($page=="add-downloads.php")?' class="active"':''; ?>><a href="?page=add-downloads.php">Add Downloads</a></li>
                        </ul>
                    </li>
					<li<?php echo ($page=="create-user.php" || $page=="all-users.php" )?' class="active"':''; ?>>
                        <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Users</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse<?php echo ($page=="create-user.php" || $page=="all-users.php")?' in':''; ?>">
							<li <?php echo ($page=="all-users.php")?' class="active"':''; ?>><a href="?page=all-users.php">All Users</a></li>
                        	<li <?php echo ($page=="create-user.php")?' class="active"':''; ?>><a href="?page=create-user.php">Add Users</a></li>
                        </ul>
                    </li>
					<li<?php echo ($page=="contact-us.php" || $page=="contact-us-single.php" )?' class="active"':''; ?>>
                        <a href="?page=contact-us.php"><i class="fa fa-envelope"></i> <span class="nav-label">Contact us</span><span class="fa arrow"></span></a> 
                    </li>
                    <li<?php echo ($page=="all-social-links.php"||$page=='add-social-link.php'||$page=='all-settings.php')?' class="active"':''; ?>>
                        <a href="#"><i class="fa fa-gear"></i> <span class="nav-label">Settings</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse<?php echo ($page=="change-password.php" || $page=='all-settings.php')?' in':''; ?>">
                        	<li <?php echo ($page=="all-settings.php")?' class="active"':''; ?>><a href="?page=all-settings.php">Other Settings</a></li>
							<li <?php echo ($page=="change-password.php")?' class="active"':''; ?>><a href="?page=change-password.php">Change Password</a></li>
                       </ul>
                    </li>
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header"><a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a></div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <!-- Split button -->
                    <div class="btn-group">
                      <button class="btn btn-default" type="button"><?php echo (isset($_SESSION['language']))?'Arabic':'English'; ?></button>
                      <button aria-expanded="false" data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li<?php echo (!isset($_SESSION['language']))?' class="active"':''; ?>><a href="change-language.php?language=english">English</a></li>
                        <li<?php echo (isset($_SESSION['language']))?' class="active"':''; ?>><a href="change-language.php?language=arabic">Arabic</a></li>
                      </ul>
                    </div>
                </li>
                <li>
                    <a href="access/logout.php"><i class="fa fa-sign-out"></i> Log out</a>
                </li>
            </ul>
        </nav>
        </div>
        <div class="wrapper wrapper-content">
            <?php
			if($adminDetails['admin_type']==2){
				$explPageAccess			=	explode(",",$adminDetails['page_access']);
				$explPageAccess			=	array_filter($explPageAccess);
				if(in_array($pageurlId['pu_id'],$explPageAccess)==1 || $page=="dashboard.php" || $page =='dashboard' || $page =='change-password.php'){
					include_once($pageName); 
				}else{
					echo '<img src="images/403.png" />';
				}
			}else{
				include_once($pageName); 
			}
		?>
      	</div>
        <div class="footer fixed">
            <div class="pull-right">
                
            </div>
            <div>
                <strong>Copyright</strong> Flagday &copy; <?php echo date("Y"); ?>
            </div>
        </div>
        </div>
    </div>
<script>
$("#side-menu > li").click(function(e) {
	/*var allChild	=	$("#side-menu > li").find("ul");
	var curr		=	$(this).find("ul");
	for(var i=0;i<allChild.length;i++){
		if(allChild[i]==curr[0]){
		}
	}*/
	$(".nav > li").removeClass("active");
	if(!$(this).hasClass("nav-header")){
		$(this).addClass("active");
	}
	$(".nav > li").children("ul").removeClass("in");
    $(this).children("ul").toggleClass("in");
});
$(".navbar-minimalize").click(function(e) {
    $("body").toggleClass("mini-navbar").after(function() {
		if(!$("body").hasClass("mini-navbar")){
			setTimeout(function(){
				$(".nav > li a span").css("visibility","visible");
			},300);
		}else{
			$(".nav > li a span").stop().delay(1500).css("visibility","hidden");
		}
        
    });;
});
</script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="ckeditor/ckfinder.js"></script>
</body>
</html>