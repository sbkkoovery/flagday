<?php 
@session_start();
$LANGUAGE='';
if(isset($_SESSION['language'])){
	$LANGUAGE=$_SESSION['language'];
}
include_once("../../class/".$LANGUAGE."categories.php");
include_once("../../class/common_class.php");
$objCategories	=	new categories();
$objCommon		=	new common();
$objCommon->adminCheck();
if(isset($_GET['value'],$_GET['catId'])&&$_GET['value']!=""&&$_GET['catId']!=""){
	$catId		=	$objCommon->esc($_GET['catId']);
	$order		=	$objCommon->esc($_GET['value']);
	$objCategories->updateField(array('cat_order'=>$order),"cat_id=$catId");
	echo $order;
}
?>