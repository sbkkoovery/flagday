<?php 
@session_start();
include_once("../class/common_class.php");
$objCommon	=	new common();
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Flag Day | Login</title>
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="middle-box text-center loginscreen  animated fadeInDown">
  <div>
    <div>
      <h1 class="logo-name"><img src="images/logo.png" alt="FMC"></h1>
    </div>
    <h3>Welcome to Flagday</h3>
    <p>Login in. To see it in action.</p>
    <?php echo $objCommon->displayMsg(); ?>
    <form class="m-t" role="form" method="post" action="access/login.php">
      <div class="form-group">
        <input class="form-control" placeholder="Username" name="username" required type="text">
      </div>
      <div class="form-group">
        <input class="form-control" placeholder="Password" name="password" required type="password">
      </div>
      <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
      <a href="#"><small>Forgot password?</small></a>
    </form>
    <p class="m-t"> <small>flagday &copy; <?php echo date("Y");?></small> </p>
  </div>
</div>

<!-- Mainly scripts --> 
<script src="js/jquery-2.js"></script> 
<script src="js/bootstrap.js"></script>
</body>
</html>