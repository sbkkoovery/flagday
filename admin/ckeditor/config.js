﻿/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
config.toolbar = 'Full';
	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbar_Full =
[
    { name: 'document',    items : [ 'Source'] },
    { name: 'basicstyles', items : [ 'Bold','Italic','Underline' ] },
    { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
    { name: 'insert',      items : [ 'Image','Table'] },
    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'] },
    { name: 'styles',      items : [ 'Styles','Format','FontSize' ] },
	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] }
    //{ name: 'colors',       items : [ 'Color', 'ShowBlocks','-','About' ] }
];
	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.height = '400px';
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';
config.filebrowserImageBrowseUrl = '../admin/ckfinder/ckfinder.html?type=thumbs';
config.filebrowserImageUploadUrl = '../admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=thumbs';
	
};
