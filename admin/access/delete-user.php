<?php 
@session_start();
include_once("../../class/admin.php");
include_once("../../class/common_class.php");
$objAdmin		 =	new admin();
$objCommon		=	new common();
$objCommon->adminCheck();
if(isset($_GET['delId'])){
	$delId			=	$objCommon->esc($_GET['delId']);
	$objAdmin->delete("aminId=$delId");
	$objCommon->addMsg("User deleted successfully",1);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>