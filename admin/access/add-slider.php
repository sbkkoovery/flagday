<?php 
@session_start();
$LANGUAGE='';
if(isset($_SESSION['language'])){
	$LANGUAGE=$_SESSION['language'];
}
include_once("../../class/".$LANGUAGE."slider.php");
include_once("../../class/common_class.php");
$objSlider		=	new slider();
$objCommon		=	new common();
$objCommon->adminCheck();
if(isset($_POST['submit'])){
	//$_POST['cat_alias']	=	$objCommon->getAlias($_POST['cat_title']);
	if(isset($_POST['sliderId'])){
		$sliderId	=	$objCommon->esc($_POST['sliderId']);
		$currDetails	=	$objSlider->getRow("slider_id=$sliderId");
		$_POST['slider_image']	=	$currDetails['slider_image'];
		if($_FILES['sliderImage']['name']!=""){
			$ext	=	strtolower(pathinfo($_FILES['sliderImage']['name'], PATHINFO_EXTENSION));
			$validImages	=	array("jpg","jpeg","gif","png");
			if(in_array($ext,$validImages)){
				$name	=	$objCommon->getAlias($_POST['slider_name']).time();
				if(file_exists("../../uploads/home_slider/".$currDetails['slider_image'])){
					unlink("../../uploads/home_slider/".$currDetails['slider_image']);
				}
				$_POST['slider_image']	=	$objCommon->addIMG($_FILES['sliderImage'],"../../uploads/home_slider/",$name,1927,804,false);
			}
		}
		$objSlider->update($_POST,"slider_id=".$sliderId);
		$objCommon->addMsg("Slider Upadated successfully",1);
	}else{
		if($_FILES['sliderImage']['name']!=""){
			$ext	=	strtolower(pathinfo($_FILES['sliderImage']['name'], PATHINFO_EXTENSION));
			$validImages	=	array("jpg","jpeg","gif","png");
			if(in_array($ext,$validImages)){
				$name	=	$objCommon->getAlias($_POST['slider_name']).time();
				$_POST['slider_image']	=	$objCommon->addIMG($_FILES['sliderImage'],"../../uploads/home_slider/",$name,1927,804,false);
			}
		}
		$objSlider->insert($_POST);
		$objCommon->addMsg("Slider added successfully",1);
		header("location:../?page=all-sliders.php");
		exit();
	}
}else{
	$objCommon->addMsg("Please tray again",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>