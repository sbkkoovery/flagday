<?php 
@session_start();
$LANGUAGE='';
if(isset($_SESSION['language'])){
	$LANGUAGE=$_SESSION['language'];
}
include_once("../../class/".$LANGUAGE."slider.php");
include_once("../../class/common_class.php");
$objSlider		=	new slider();
$objCommon		=	new common();
$objCommon->adminCheck();
if(isset($_GET['sliderId'])){
	$sliderId		=	$objCommon->esc($_GET['sliderId']);
	$sliderDetails	=	$objSlider->getRow("slider_id=$sliderId");
	if(file_exists("../../uploads/sliders/".$sliderDetails['slider_image'])){
		unlink("../../uploads/sliders/".$sliderDetails['slider_image']);
	}
	$objSlider->delete("slider_id=$sliderId");
	$objCommon->addMsg("Slider deleted successfully",1);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>