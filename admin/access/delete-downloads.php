<?php 
@session_start();
$LANGUAGE='';
if(isset($_SESSION['language'])){
	$LANGUAGE=$_SESSION['language'];
}
include_once("../../class/".$LANGUAGE."downloads.php");
include_once("../../class/common_class.php");
$objSlider		=	new downloads();
$objCommon		=	new common();
$objCommon->adminCheck();
if(isset($_GET['sliderId'])){
	$sliderId		=	$objCommon->esc($_GET['sliderId']);
	$sliderDetails	=	$objSlider->getRow("d_id=$sliderId");
	if(file_exists("../../uploads/downloads/".$sliderDetails['d_image'])){
		unlink("../../uploads/downloads/".$sliderDetails['d_image']);
	}
	$objSlider->delete("d_id=$sliderId");
	$objCommon->addMsg("Downloads deleted successfully",1);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>