<?php 
@session_start();
$LANGUAGE='';
if(isset($_SESSION['language'])){
	$LANGUAGE=$_SESSION['language'];
}
include_once("../../class/settings.php");
include_once("../../class/common_class.php");
$objSettings	=	new settings();
$objCommon		=	new common();
$objCommon->adminCheck();
if(isset($_POST['setting_link'])&&$_POST['setting_link']!=""){	
   $objSettings->update($_POST,'setting_id=1');
   $objCommon->addMsg("Updated Successfully",1);
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>