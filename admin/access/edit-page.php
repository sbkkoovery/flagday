<?php 
@session_start();
$LANGUAGE='';
if(isset($_SESSION['language'])){
	$LANGUAGE=$_SESSION['language'];
}
include_once("../../class/".$LANGUAGE."pages.php");
include_once("../../class/common_class.php");
$objPages		=	new pages();
$objCommon		=	new common();
$objCommon->adminCheck();
if(isset($_POST['page_title'])&&$_POST['page_content']!=""){
	if(isset($_POST['pageId'])){
		$_POST['page_alias']	=	$objCommon->getAlias($_POST['page_title']);
		$pageId	=	$objCommon->esc($_POST['pageId']);
		$objPages->update($_POST,"page_id=$pageId");
		$objCommon->addMsg("Page updated successfully",1);
	}else{
		$_POST['page_alias']	=	$objCommon->getAlias($_POST['page_title']);
		$objPages->insert($_POST);
		$objCommon->addMsg("Page added successfully",1);
		header("location:../?page=all-pages.php");
		exit();
	}
}else{
	$objCommon->addMsg("Please enter fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>