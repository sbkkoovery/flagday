<?php 
@session_start();
include_once("../../class/admin.php");
include_once("../../class/common_class.php");
$objAdmin		 =	new admin();
$objCommon		=	new common();
$objCommon->adminCheck();
if(($_POST['username'] !='' && $_POST['password']!="") || $_POST['editId'] !=''){
	$_POST['admin_type']		=	2;
	$_POST['admin_name']		=	$objCommon->esc($_POST['admin_name']);
	$_POST['email']		  	 =	$objCommon->esc($_POST['email']);
	$emirate					=	$_POST['emirate'];
	$pages					  =	$_POST['pages'];
	if(count($emirate)>0){
		foreach($emirate as $keyEmirates=>$allEmirates){
			if($keyEmirates ==0){
				$emiratesStr   .=	",".$allEmirates.",";
			}else{
				$emiratesStr   .=	$allEmirates.",";
			}
		}
		$_POST['flag_access']   =	$emiratesStr;
	}
	if(count($pages)>0){
		foreach($pages as $keyPages=>$allPages){
			if($keyPages ==0){
				$pagesStr   	   .=	",".$allPages.",";
			}else{
				$pagesStr   		.=	$allPages.",";
			}
		}
		$_POST['page_access']   =	$pagesStr;
	}
	if(isset($_POST['editId'])){
		$objAdmin->update($_POST,"aminId=".$_POST['editId']);
		$objCommon->addMsg("User updated successfully",1);
		header("location:../?page=all-users.php");
		exit();
	}else{
		$_POST['username']		  =	$objCommon->esc($_POST['username']);
		$_POST['password']		  =	md5($objCommon->esc($_POST['password']));
		$objAdmin->insert($_POST);
		$objCommon->addMsg("User added successfully",1);
		header("location:../?page=all-users.php");
		exit();
	}
}else{
	$objCommon->addMsg("Please enter fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>