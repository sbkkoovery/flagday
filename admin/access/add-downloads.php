<?php 
@session_start();
$LANGUAGE='';
if(isset($_SESSION['language'])){
	$LANGUAGE=$_SESSION['language'];
}
include_once("../../class/".$LANGUAGE."downloads.php");
include_once("../../class/common_class.php");
$objSlider		=	new downloads();
$objCommon		=	new common();
$objCommon->adminCheck();
if(isset($_POST['submit'])){
	//$_POST['cat_alias']	=	$objCommon->getAlias($_POST['cat_title']);
	if(isset($_POST['sliderId'])){
		$sliderId	=	$objCommon->esc($_POST['sliderId']);
		$currDetails	=	$objSlider->getRow("d_id=$sliderId");
		$_POST['d_image']	=	$currDetails['d_image'];
		if($_FILES['sliderImage']['name']!=""){
			$ext	=	strtolower(pathinfo($_FILES['sliderImage']['name'], PATHINFO_EXTENSION));
			$validImages	=	array("jpg","jpeg","gif","png","doc","docx","pdf","mp4");
			if(in_array($ext,$validImages)){
				$name	=	$objCommon->getAlias($_POST['d_name']).time();
				if(file_exists("../../uploads/downloads/".$currDetails['d_image'])){
					unlink("../../uploads/downloads/".$currDetails['d_image']);
				}
				if(move_uploaded_file($_FILES['sliderImage']['tmp_name'],'../../uploads/downloads/'.$name.'.'.$ext))
				{
					$_POST['d_image']	=	$name.'.'.$ext;
				}
			}
		}
		$objSlider->update($_POST,"d_id=".$sliderId);
		$objCommon->addMsg("Downloads Upadated successfully",1);
	}else{
		if($_FILES['sliderImage']['name']!=""){
			$ext	=	strtolower(pathinfo($_FILES['sliderImage']['name'], PATHINFO_EXTENSION));
			$validImages	=	array("jpg","jpeg","gif","png","doc","docx","pdf","mp4");
			if(in_array($ext,$validImages)){
				$name	=	$objCommon->getAlias($_POST['d_name']).time();
				if(move_uploaded_file($_FILES['sliderImage']['tmp_name'],'../../uploads/downloads/'.$name.'.'.$ext))
				{
					$_POST['d_image']	=	$name.'.'.$ext;
				}
			}
		}
		$objSlider->insert($_POST);
		$objCommon->addMsg("Downloads added successfully",1);
		header("location:../?page=all-downloads.php");
		exit();
	}
}else{
	$objCommon->addMsg("Please tray again",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>